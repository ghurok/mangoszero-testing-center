<?php
include 'inc/include.php';

$latest_reports = new Report;
$latest_reports->getReports(null, false, false, true, null, '`id` DESC', '10');

$statistics = new Statistics;
$statistics->getStatisticsInDb();
$statistics->getNumOnlinePlayers();

?>
<!DOCTYPE html>
<html>
<head>
	<title>Testing Center - Project Silverpine</title>
	<link rel="stylesheet" type="text/css" href="css/tracker.css">
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/tabs.js"></script>
</head>
<body>
	<div id="t_wrapper">
	<?php include 'inc/menu.php'; ?>
	<?php include 'inc/precontents.php'; ?>
		<div id="content-wrapper">
			<section id="quickfacts">
				<h2>Quick Stats</h2>
				<ul>
					<li>Players Online: <?php echo $statistics->statistics['numOnlinePlayers']; ?></li>
				</ul>
				<ul>
					<li>In-Game Characters: <?php echo $statistics->statistics['numCharacters']; ?></li>
					<li>In-Game Accounts: <?php echo $statistics->statistics['numAccounts']; ?></li>
					<li>Reports: <?php echo $statistics->statistics['numReports']; ?></li>
				</ul>
			</section>
			<div id="content-left">
				<h1>Welcome to the Testing Center</h1>
				<p>The Testing Center is used for tracking testing progress on the <a href="http://project-silverpine.com">Project Silverpine</a> game server.</p>
				<h2>About Project Silverpine</h2>
				<p>Project Silverpine is an open source Classic World of Warcraft project with the intention of replicating the game as it was at patch 1.12.1, just before the release of The Burning Crusade. This means that by the end of this project we intend to have each and every feature in the game working 100% as it was on retail, right down to the finest details (with the exception of obvious bugs). Our server is meant for testing rather than "proper" play.</p>
				<p>Read more about our project <a href="http://project-silverpine.com/aboutus">here</a>.</p>
				<h2>Tracking &amp; Progress</h2>
				<p>The reason we want you to <span class="bold">not only</span> submit reports if you've found bugs but also if something is working as intended is so that we know what has or hasn't been tested in the game. Not only does this help the developers but it also helps players. They need to know what hasn't been tested yet so that they can test it. The testing part of the project will be considered finished when all aspects of the game have been tested thoroughly. Only submitting bug reports will not help us reach our final goal.</p>
				<h2>How to contribute</h2>
				<p>First of you need to create an account on our game server. You can do so <a href="">here</a>. When you have done so you can log in to the Testing Center. This will allow you to submit testing reports to developers (and the public).</p>
				<p>We encourage you to use the official Project Silverpine game server to do your testing. You can find a guide on how to right <a href="">here</a>. Log in and play the game in whatever way you'd like to. Don't forget to submit testing reports!</p>
				<p>It should be noted that we do our testing in different so called <span class="italic">phases</span>. You can read more about them and see our progress on <a href="phases.php">this page</a>.</p>
				<h2>Technical Support</h2>
				<p>Having any trouble with the Testing Center? Post on our <a href="http://project-silverpine.com/forums">forums</a> or send us an email at <a href="mailto:support@project-silverpine.com">support@project-silverpine.com</a>.</p>
				<h2>Have any more questions?</h2>
				<p>Find more questions and answers in our FAQ. If you have any other questions don't forget to visit our <a href="http://project-silverpine.com/forums">forums</a>.</p>
			</div>
			<div id="related">
				<div id="tabs-container">
					<ul>
						<li><a href="#1" class="showSingle" data-target="1">Latest reports (<?php echo $latest_reports->numResults; ?>)</a></li>
					</ul>
					<div class="clear"></div>
				</div>
				<div id="tabs-content">
					<div id="tabs-content-1" class="targetDiv">
					<table id="latest-report-table">
					<?php if($latest_reports->numResults > 0): ?>
					<th><div><span>Link</span></div></th><th><div><span>Date</span></div></th><th><div><span>Type</span></div></th><th><div><span>Status</span></div></th><th><div><span>Comment</span></div></th><th><div><span>Submitted by</span></div></th>
						<?php foreach($latest_reports->report as $row): ?>
								<tr><td><a href="<?php echo $row['type_name']; ?>.php?<?php echo $row['type_name']; ?>=<?php echo $row['entry']; ?>">Link</a></td><td><?php echo $row['date']; ?></td><td><?php echo ucfirst($row['type_name']); ?></td><td class="status-td"><?php echo $row['status']; ?></td><td class="normal"><?php echo $row['comment']; ?></td><td><a href="profile.php?user=<?php echo $row['user_id']; ?>"><?php echo $user->id2nick($row['user_id']); ?></a></td></tr>
						<?php endforeach; ?>
					<?php else: ?>
						<p>No reports have been made by <?php echo $user->user[0]['username']; ?></p>
					<?php endif; ?>
					</table>
					</div>
					<div class="clear"></div>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</body>
</html>