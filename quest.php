<?php
	include 'inc/include.php';

	$quest = new Quest;
	$report = new Report;

	$t_redirect = new Redirect;

	if(isset($_POST['submit_report']))
		$report->insertReport(QUEST);

	if(isset($_POST['delete_report']))
		$report->deleteReport();

	if(isset($_POST['fixed_report']))
		$report->fixedReport();
	
	if(!empty($_GET['quest']) && is_numeric($_GET['quest']))
	{
		$quest->getQuests(2, null, 'entry='.$_GET['quest']);
		$quest->getStatus($_GET['quest']);
		$report->getReports(null, $_GET['quest'], QUEST, true);
	?>
	<head>
		<title>Quest: <?php echo $quest->quest[0]['Title']; ?> (<?php echo $quest->quest[0]['entry']; ?>)</title>
		<link rel="stylesheet" type="text/css" href="css/tracker.css">
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/tabs.js"></script>
	</head>
	<?php
	}else
	{
		//$t_redirect->doRedirect('tracker/redirect.php?redirect='.'quests', 0, null, 2);
		//exit($t_redirect->doRedirect($t_redirect->currentUrl()));
	}

	$quest->getQuestWebLinks(array('Title' => $quest->quest[0]['Title'], 'entry' => $quest->quest[0]['entry']));
?>
<div id="t_wrapper">
	<?php include 'inc/menu.php'; ?>
	<?php include 'inc/precontents.php'; ?>
	<div id="content-wrapper">
		<div id="main-content">
			<section id="quickfacts">
				<h2>Quick Facts</h2>
				<ul>
					<li>Level: <?php echo $quest->quest[0]['QuestLevel']; ?></li>
					<li>Requires level <?php echo $quest->quest[0]['MinLevel']; ?></li>
					<?php if($quest->quest[0]['Type'] != false): ?><li>Type: <?php echo $quest->quest[0]['Type']; ?></li><?php endif; ?>
					<?php if($quest->quest[0]['RequiredSide'] != false): ?><li>Side: <?php echo $quest->quest[0]['RequiredSide']; ?></li><?php endif; ?>
					<?php if($quest->quest[0]['QuestStart'] != false): ?><li><span class="icontiny quest_start">Start: <?php echo $quest->quest[0]['QuestStart']; ?></li><?php endif; ?>
					<?php if($quest->quest[0]['QuestEnd'] != false): ?><li><span class="icontiny quest_end">End: <?php echo $quest->quest[0]['QuestEnd']; ?></li><?php endif; ?>
					<?php if($quest->quest[0]['PrevQuest'] != false): ?><li>Previous: <?php echo $quest->quest[0]['PrevQuest']; ?></li><?php endif; ?>
					<?php if($quest->quest[0]['NextQuest'] != false): ?><li>Next: <?php echo $quest->quest[0]['NextQuest']; ?></li><?php endif; ?>
					<li><?php echo $quest->quest[0]['Sharable']; ?></li>
				</ul>
			</section>
			<div id="content-left">
				<h1><?php echo $quest->quest[0]['Title']; ?> (<?php echo $quest->quest[0]['entry']; ?>) - <?php echo $quest->status; ?></h1>
				<?php if($quest->quest[0]['Objectives'] != ''): ?><p class="quest_text"><?php echo $quest->quest[0]['Objectives']; ?></p><?php endif; ?>
				<?php if($quest->quest[0]['Details'] != ''): ?><h2>Description</h2>
				<p class="quest_text"><?php echo $quest->quest[0]['Details']; ?></p><?php endif; ?>
				<?php if($quest->quest[0]['RequestItemsText'] != ''): ?><h2>Progress</h2>
				<p class="quest_text"><?php echo $quest->quest[0]['RequestItemsText']; ?></p><?php endif; ?>
				<?php if($quest->quest[0]['OfferRewardText'] != ''): ?><h2>Completion</h2>
				<p class="quest_text"><?php echo $quest->quest[0]['OfferRewardText']; ?></p><?php endif; ?>
				<h2>Rewards</h2>
				You will receive: <?php echo $quest->quest[0]['RewOrReqMoney']; ?>  (or <?php echo $quest->quest[0]['RewMoneyMaxLevel']; ?> if completed at level 60)
				<h2>Gains</h2>
				<p class="quest_text">Upon completion of this quest you will gain:</p>
				<ul>
					<?php if($quest->quest[0]['XP_Reward'] > 0): ?><li><?php echo $quest->quest[0]['XP_Reward']; ?> experience</li><?php endif; ?>
					<?php if(count($quest->quest[0]['Reputation']) > 0): ?>
						<?php foreach($quest->quest[0]['Reputation'] as $row): ?>
							<li><?php echo $row['reputation']; ?> reputation with <a href=""><?php echo $row['faction']; ?></a></li>
						<?php endforeach; ?>
					<?php endif; ?>
				</ul>
			</div>
		</div>
		<div id="related">
			<div id="tabs-container">
				<ul>
					<li><a href="#comments" class="showSingle" data-target="1">Reports (<?php echo $report->numResults; ?>)</a></li>
					<li><a href="#links" class="showSingle" data-target="2">Links</a></li>
				</ul>
				<div class="clear"></div>
			</div>
			<div id="tabs-content">
				<div id="tabs-content-1" class="targetDiv">
				<div id="left-content">
					<?php if($report->numResults >= 1): ?>
						<?php foreach($report->report as $row): ?>
								<article id="comment">
								<?php if($user->gmlevel() > 0): ?>
								<div class="h-comment-admin">
									<span class="h-comment-left">
										<form class="admin_panel_delete" method="post">
											<input type="hidden" name="report_id" value="<?php echo $row['id']; ?>" />
											<input type="submit" name="delete_report" class="delete_report" value="" />
										</form>
									</span>
									<span class="h-comment-right">
										<form class="admin_panel_fixed" method="post">
											<select name="report_fix_by" class="report_fix_by">
												<option value="0">Fix by <?php echo $user->id2nick(@$_SESSION['user_id']); ?></option>
												<option value="1">Fix by someone else</option>
											</select>
											<input type="hidden" name="report_id" value="<?php echo $row['id']; ?>" />
											<input type="submit" name="fixed_report" class="fixed_report" value="" />
										</form>
									</span>
								</div>
								<?php elseif($row['user_id'] == @$_SESSION['user_id'] && @$_SESSION['user_id'] > 0 && $row['user_id'] > 0): ?>
								<div class="h-comment-admin">
									<span class="h-comment-left">
										<form class="admin_panel_delete" method="post">
											<input type="hidden" name="report_id" value="<?php echo $row['id']; ?>" />
											<input type="submit" name="delete_report" class="delete_report" value="" />
										</form>
									</span>
								</div>
								<?php endif; ?>
								<div class="h-comment">
									<span class="h-comment-right">
										<span class="bold"><?php echo $row['status']; ?></span>
									</span>
									<?php if($row['fix_by_user_id'] != NULL): ?>
										<span class="h-comment-left">
											<?php if($row['fix_by_user_id'] != -1): ?>
												<span>Fixed by <span class="bold"><a href="profile.php?user=<?php echo $row['fix_by_user_id']; ?>"><?php echo $user->id2nick($row['fix_by_user_id']); ?></a></span></span>
											<?php else: ?>
												<span>Fixed by unknown developer</span>
											<?php endif; ?>
										</span>
									<?php endif; ?>
								</div>
								<section>
									<p class="comment"><?php echo $row['comment']; ?></p>
									<footer>
										<span class="f-comment-left">
											<p class="reported_by">Posted by <span class="bold"><a href="profile.php?user=<?php echo $row['user_id']; ?>"><?php echo $user->id2nick($row['user_id']); ?></a></span> on <span class="bold"><?php echo $row['date']; ?></span></p>
										</span>
										<span class="f-comment-right">
											<p><span class="db_rev_comment"><?php echo $row['db_rev']; ?></span></p>
										</span>
										<div class="clear"></div>
									</footer>
								</section>
								</article>
						<?php endforeach; ?>
					<?php else: ?>
						<p id="no_reports">There are no reports.</p>
					<?php endif; ?>
					<?php if(isset($_SESSION["user_id"])): ?>
					<form action="#" method="post" name="report_form" id="report_form">
						<p>Status:</p>
						<select name="report_status">
							<option value="<?php echo DISCUSSION; ?>">Discussion</option>
							<option value="<?php echo QUEST_BUGS_FOUND; ?>"><?php echo BUGS_FOUND; ?></option>
							<option value="<?php echo QUEST_COMPLETABLE_NO_BUGS_FOUND; ?>"><?php echo COMPLETABLE_NO_BUGS_FOUND; ?></option>
							<option value="<?php echo QUEST_NOT_COMPLETABLE; ?>"><?php echo NOT_COMPLETABLE; ?></option>
							<option value="<?php echo QUEST_OBSOLETE; ?>"><?php echo OBSOLETE; ?></option>
							<option value="<?php echo QUEST_WORKING_PERFECTLY; ?>"><?php echo WORKING_PERFECTLY; ?></option>
						</select>
						<p>Comment:</p>
						<textarea name="report_comment"><?php echo @$_SESSION['report_comment']; ?></textarea><br />
						<input type="checkbox" name="read_ins" id="read_ins" value="true"> I have read the instructions!<br />
						<input type="submit" name="submit_report" value="Submit" />
					</form>
					<?php else: ?>
						<p>Log in to make a report.</p>
					<?php endif; ?>
				</div>
				<div id="right-content">
				<div id="instructions">
					<h2>Instructions (read before posting)</h2>
					<section>
					<p>
					Do not report drop rate/drop location bugs (except quest quests drops, which should be reported at the relevant quest entry instead).
					If you don't know where to report a bug, use your own judgment. We'd rather get a bug report in the wrong category than no bug report at all.
					</p>
					</section>
					<section>
					<h4 class="status_white">Discussion</h4>
					<p>Not really a status. This is used for when you want to discuss something.</p>
					</section>
					<section>
					<span class="tag tag2"><?php echo NOT_COMPLETABLE; ?></span>
					<p>The quest can not be completed.</p>
					</section>
					<section>
					<span class="tag tag2"><?php echo BUGS_FOUND; ?></span>
					<p>The quest is bugged is some aspect (wrong drop rate, wrong drop location, wrong mob/object respawn time).</p>
					</section>
					<section>
					<span class="tag tag4"><?php echo COMPLETABLE_NO_BUGS_FOUND; ?></span>
					<p>A basic check revealed no bugs and the quest is completable.</p>
					</section>
					<section>
					<span class="tag tag2"><?php echo OBSOLETE; ?></span>
					<p>The quest is not available in-game AND shouldn't be available in-game. If it is available, report it as <?php echo BUGS_FOUND; ?>.</p>
					</section>
					<section>
					<span class="tag tag8"><?php echo WORKING_PERFECTLY; ?></span>
					<ul>
					<p class="bold">Compare the following:</p>
				    <li>Quest level</li>
				    <li>Minimum level</li>
				    <li>Text (3)</li>
				    <li>Proper reward display (the reward items offered should almost always be visible when you first accept the quest. Not sure if the XP to gold at 60 conversion money should be included in the “gold reward” entry when you accept the quest).</li>
				    <li>XP reward</li>
				    <li>Item reward (bugs with the actual reward items should be reported on the items page)</li>
				    <li>Repeatability if repeatable</li>
				    <li>Emotes, chat text and abilities used by the quest giver before or after the quest (if the NPC or any relevant mobs don't use the proper abilities while in combat during the quest, report it on the individual NPCs page).</li>
				    <li>The path traveled by the NPC during an escort quest</li>
				    <li>Scripted events (number of mobs spawned, emotes used and so forth)</li>
					<p class="bold">to a reliable pre-tbc source. Links are required.</p>
					</ul>
					</section>
				</div>
				</div>
				</div>
				<div id="tabs-content-2" class="targetDiv">
					<ul id="list_links">
						<li><?php echo $quest->quest['WebLinks']['WB_GoblinWorkshop']; ?></li>
						<li><?php echo $quest->quest['WebLinks']['WB_Thottbot']; ?></li>
						<li><?php echo $quest->quest['WebLinks']['WB_Allakhazam']; ?></li>
						<li><?php echo $quest->quest['WebLinks']['Old_Wowhead']; ?></li>
						<li><?php echo $quest->quest['WebLinks']['Wowd']; ?></li>
						<li><?php echo $quest->quest['WebLinks']['Wowpedia']; ?></li>
					</ul>
				</div>
				<div class="clear"></div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>