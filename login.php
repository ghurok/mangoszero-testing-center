<?php
	include 'inc/include.php';
	$user = new User;

	if(isset($_POST['login_submit']))
		$user->login();

?>
<html>
<head>
	<title>Log in</title>
	<link rel="stylesheet" type="text/css" href="css/tracker.css">
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/tabs.js"></script>
</head>
<body>
<div id="t_wrapper">
	<?php include 'inc/menu.php'; ?>
	<?php include 'inc/precontents.php'; ?>
	<div id="content-wrapper">
		<div id="main-content">
				<section id="quickfacts">
					<h2>Quick Facts</h2>
					<ul>
						<li>Game Accounts are used.</li>
					</ul>
				</section>
				<div id="content-left">
					<h1>Log in</h1>
					<p>Don't have a game account? <a href="signup.php">Sign up</a>.</p>
					<form id="login_form" method="post">
						<p>Username</p>
						<input type="text" name="login_username" required>
						<p>Password</p>
						<input type="password" name="login_password" required>
						<input type="submit" name="login_submit" id="login_submit" value="Login">
					</form>
				</div>
		</div>
		<div class="clear"></div>
	</div>
</div>