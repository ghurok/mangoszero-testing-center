<?php include 'inc/include.php'; ?>
<head>
	<title>Items</title>
	<link rel="stylesheet" type="text/css" href="css/tracker.css">
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/tabs.js"></script>
</head>

<div id="t_wrapper">
	<?php include 'inc/menu.php'; ?>
	<?php include 'inc/precontents.php'; ?>
	<div id="content-wrapper">

<?php
	$itemTotal = new Item;
	$itemTotal->getItems($list = true, 'entry LIKE "%'.@$_GET['q'].'%" OR name LIKE "%'.@$_GET['q'].'%"');
	if($itemTotal->numResults > 0)
	{
		$pages = new Paginator;
		$pages->items_total = $itemTotal->numResults;
		$pages->mid_range = 9;
		$pages->paginate();
	}else
	{
		die('<h1>No results found!</h1><p>No results could be found. Try another keyword!</p>');
	}
?>
<?php if(!isset($_GET['q'])): ?>
<h1>Items (<?php echo $itemTotal->numResults; ?>)</h1>
<?php else: ?>
<h1>Items matching '<?php echo $_GET['q']; ?>' (<?php echo $itemTotal->numResults; ?>)</h1>
<?php endif; ?>

	<?php
	$item = new Item;
	$item->getItems($list = true, 'entry LIKE "%'.@$_GET['q'].'%" OR name LIKE "%'.@$_GET['q'].'%"', null, $pages->limit);?>
		
		<div id="pag_options">
			<div id="pag_left">
				Showing <span class="bold"><?php echo $pages->show_min_out_of; ?></span> - <span class="bold"><?php echo $pages->show_max_out_of; ?></span> of <span class="bold"><?php echo $itemTotal->numResults; ?></span></p>
			</div>
			<div id="pag_right">
				<div id="items_per_page"><?php echo $pages->display_items_per_page(); ?></div>
				<div id="jump_menu"><?php echo $pages->display_jump_menu(); ?></div>
			</div>
		</div>

<div class="paginate_pages"><?php echo $pages->display_pages(); ?></div>

<table id="entries">
<th><div><span>ID</span></div></th><th><div><span>Item</span></div></th><th><div><span>Status</span></div></th><th><div><span>Reports</span></div></th>
<?php foreach($item->item as $row): ?>
	<?php
	$report = new Report;
	$item = new Item;
	$report->getReports(1, $row['entry'], ITEM);
	$item->getStatus($row['entry']);
	?>
	<tr>
		<td><?php echo $row['entry']; ?></td>
		<td><img class="entry_icon" src="<?php echo $item->getIcon($row['displayid']);?>" /><a href="item.php?item=<?php echo $row['entry']; ?>" style="color: <?php echo $row['item_color']; ?>;"><?php echo $row['name']; ?></a></td>
		<td><?php echo $item->status; ?></td>
		<td><?php echo $report->numResults; ?></td>
	</tr>
<?php endforeach; ?>
</table>
</div>
</div>