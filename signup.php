<?php
	include 'inc/include.php';
	$user = new User;

	if(isset($_POST['signup_submit']))
		$user->signup();

?>
<html>
<head>
	<title>Sign up</title>
	<link rel="stylesheet" type="text/css" href="css/tracker.css">
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/tabs.js"></script>
</head>
<body>
<div id="t_wrapper">
	<?php include 'inc/menu.php'; ?>
	<?php include 'inc/precontents.php'; ?>
	<div id="content-wrapper">
		<div id="main-content">
				<section id="quickfacts">
					<h2>Account Rules</h2>
					<ul>
						Username
						<li>Only A-Z and 0-9 are allowed.</li>
						<li>Max 16 characters.</li>
						Password
						<li>Max 16 characters.</li>
					</ul>
				</section>
				<div id="content-left">
					<h1>Sign up</h1>
					<p>Game accounts are used in the Testing Center.</p>
					<p>The account information you fill in here will be used for your game accounts as well.</p>
					<form id="signup_form" method="post">
						<p>Username</p>
						<input type="text" name="signup_username" required>
						<p>Password</p>
						<input type="password" name="signup_password" required>
						<p>Password again</p>
						<input type="password" name="signup_password_again" required>
						<input type="submit" name="signup_submit" id="signup_submit" value="Sign up">
					</form>
				</div>
		</div>
		<div class="clear"></div>
	</div>
</div>