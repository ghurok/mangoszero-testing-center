<?php include 'inc/include.php'; ?>
<head>
	<title>Objects</title>
	<link rel="stylesheet" type="text/css" href="css/tracker.css">
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/tabs.js"></script>
</head>

<div id="t_wrapper">
	<?php include 'inc/menu.php'; ?>
	<?php include 'inc/precontents.php'; ?>
	<div id="content-wrapper">

<?php
	$objectTotal = new Object;
	$objectTotal->getObjects($list = true, 'entry LIKE "%'.@$_GET['q'].'%" OR name LIKE "%'.@$_GET['q'].'%"');
	if($objectTotal->numResults > 0)
	{
		$pages = new Paginator;
		$pages->items_total = $objectTotal->numResults;
		$pages->mid_range = 9;
		$pages->paginate();
	}else
	{
		die('<h1>No results found!</h1><p>No results could be found. Try another keyword!</p>');
	}
?>
<?php if(!isset($_GET['q'])): ?>
<h1>Objects (<?php echo $objectTotal->numResults; ?>)</h1>
<?php else: ?>
<h1>Objects matching '<?php echo $_GET['q']; ?>' (<?php echo $objectTotal->numResults; ?>)</h1>
<?php endif; ?>

	<?php
	$object = new Object;
	$object->getObjects($list = true, 'entry LIKE "%'.@$_GET['q'].'%" OR name LIKE "%'.@$_GET['q'].'%"', null, $pages->limit);?>
		
		<div id="pag_options">
			<div id="pag_left">
				Showing <span class="bold"><?php echo $pages->show_min_out_of; ?></span> - <span class="bold"><?php echo $pages->show_max_out_of; ?></span> of <span class="bold"><?php echo $objectTotal->numResults; ?></span></p>
			</div>
			<div id="pag_right">
				<div id="items_per_page"><?php echo $pages->display_items_per_page(); ?></div>
				<div id="jump_menu"><?php echo $pages->display_jump_menu(); ?></div>
			</div>
		</div>

<div class="paginate_pages"><?php echo $pages->display_pages(); ?></div>

<table id="entries">
<th><div><span>ID</span></div></th><th><div><span>Object</span></div></th><th><div><span>Guids</span></div></th><th><div><span>Status</th><th><div><span>Reports</span></div></th>
<?php foreach($object->object as $row): ?>
	<?php
	$report = new Report;
	$object = new Object;
	$object_guid = new Object;
	$object_guid->getGuids($row['entry']);
	$report->getReports(1, $row['entry'], _OBJECT);
	$object->getStatus($row['entry']);
	?>
	<tr>
		<td><?php echo $row['entry']; ?></td>
		<td><a href="object.php?object=<?php echo $row['entry']; ?>"><?php echo $row['name']; ?></a></td>
		<td><?php echo $object_guid->numResults; ?></td>
		<td><?php echo $object->status; ?></td>
		<td><?php echo $report->numResults; ?></td>
	</tr>
<?php endforeach; ?>
</table>
</div>
</div>