<?php
	include 'inc/include.php';

$zone = new Area;

$phase = new Phase;
$quest = new Quest;

$zone->getSubZones(array($_GET['zone']));

$data = array('zoneorsort' => $zone->quest_zoneorsort, 'RequiredClasses' => null);

$quest->getQuests(1, $data);

$zone->getZoneInfoByZones(null, array($_GET['zone']));
?>
<html>
<head>
	<title>Zone: <?php echo $zone->zones_list[0]['zone_name']; ?> (<?php echo $zone->zones_list[0]['id']; ?>)</title>
	<link rel="stylesheet" type="text/css" href="css/tracker.css">
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/tabs.js"></script>
</head>
<body>
<div id="t_wrapper">
	<?php include 'inc/menu.php'; ?>
	<?php include 'inc/precontents.php'; ?>
	<div id="content-wrapper">
		<div id="main-content">
			<section id="quickfacts">
				<h2>Quick Facts</h2>
				<ul>
					<li>Quests: <?php echo $zone->zones_list[0]['numQuests']; ?></li>
					<li>Sub-zones: <?php echo $zone->zones_list[0]['numSubZones']; ?></li>
					<li>Quest Progress (Tested): <?php echo $zone->zones_list[0]['questProgPct']; ?>%</li>
				</ul>
			</section>
			<div id="content-left">
				<h1><?php echo $zone->zones_list[0]['zone_name']; ?> (<?php echo $zone->zones_list[0]['id']; ?>)</h1>
			</div>
		</div>
		<div id="related">
			<div id="tabs-container">
				<ul>
					<li><a href="#comments" class="showSingle" data-target="1">Quests (<?php echo $zone->zones_list[0]['numQuests']; ?>)</a></li>
				</ul>
				<div class="clear"></div>
			</div>
			<div id="tabs-content">
				<div id="tabs-content-1" class="targetDiv">
					<table id="latest-report-table">
					<?php if($quest->numResults > 0): ?>
					<th><div><span>ID</span></div></th><th><div><span>Quest</span></div></th><th><div><span>Status</span></div></th><th><div><span>Reports</span></div></th>
					<?php foreach($quest->quest as $row): ?>
						<?php
						$report = new Report;
						$report->getReports(1, $row['entry'], QUEST);
						$quest->getStatus($row['entry']);
						?>
						<tr>
							<td><?php echo $row['entry']; ?></td>
							<td><a href="quest.php?quest=<?php echo $row['entry']; ?>"><?php echo $row['Title']; ?></a></td>
							<td><?php echo $quest->status; ?></td>
							<td><?php echo $report->numResults; ?></td>
						</tr>
					<?php endforeach; ?>
					<?php else: ?>
						<p>There are no quests in this zone.</p>
					<?php endif; ?>
					</table>
				</div>
				<div class="clear"></div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>