$(document).ready(function() {
	$('.showSingle').on('click', function () {
		$('.showSingle').removeClass('selected');
	    $(this).addClass('selected').siblings().removeClass('selected');
	    $('.targetDiv').hide();
	    $('#tabs-content-' + $(this).data('target')).show();
	});
	$('.showSingle').first().click();
});