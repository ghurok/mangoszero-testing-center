<?php
include 'inc/include.php';

$dungeons = new Area;
$raids = new Area;

$phase = new Phase;
$quest = new Quest;

$dungeons->getZoneInfoByZones(null, $dungeons->getDungeonsZoneId());
$raids->getZoneInfoByZones(null, $raids->getRaidsZoneId());

?>
<!DOCTYPE html>
<html>
<head>
	<title>Instances</title>
	<link rel="stylesheet" type="text/css" href="css/tracker.css">
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/tabs.js"></script>
</head>
<body>
	<div id="t_wrapper">
		<?php include 'inc/menu.php'; ?>
		<?php include 'inc/precontents.php'; ?>
		<div id="content-wrapper">
			<h1>Instances</h1>
			<div id="related">
				<div id="tabs-container">
					<ul>
						<li><a href="#1" class="showSingle" data-target="1">Dungeons</a></li>
						<li><a href="#2" class="showSingle" data-target="2">Raids</a></li>
					</ul>
					<div class="clear"></div>
				</div>
				<div id="tabs-content">
					<div id="tabs-content-1" class="targetDiv">
						<table>
							<th><div><span>Dungeon</span></div></th><th><div><span>Quests</span></div></th><th colspan="2"><div><span>Quest Progress (Tested)</span></div></th>
							<?php foreach($dungeons->zones_list as $row): ?>
								<tr><td><a href="zone.php?zone=<?php echo $row['id']; ?>"><?php echo $row['zone_name']; ?></a></td><td><?php echo $row['numQuests']; ?></td><td><div class="meter"><span style="width: <?php echo $row['questProgPct']; ?>%"></span></div></td><td class="tb-quest-prog-pct"><?php echo $row['questProgPct']; ?>%</td></tr>
							<?php endforeach; ?>
							<tr><td><span class="bold">Total</span></td><td><span class="bold"><?php echo $dungeons->zones_total['numQuests']; ?></span></td><td><div class="meter"><span style="width: <?php echo $dungeons->zones_total['questProgPct']; ?>%"></span></div></td><td class="tb-quest-prog-pct"><span class="bold"><?php echo $dungeons->zones_total['questProgPct']; ?>%</span></td></tr>
						</table>
					</div>
					<div id="tabs-content-2" class="targetDiv">
						<table>
							<th><div><span>Raid</span></div></th><th><div><span>Quests</span></div></th><th colspan="2"><div><span>Quest Progress (Tested)</span></div></th>
							<?php foreach($raids->zones_list as $row): ?>
								<tr><td><a href="zone.php?zone=<?php echo $row['id']; ?>"><?php echo $row['zone_name']; ?></a></td><td><?php echo $row['numQuests']; ?></td><td><div class="meter"><span style="width: <?php echo $row['questProgPct']; ?>%"></span></div></td><td class="tb-quest-prog-pct"><?php echo $row['questProgPct']; ?>%</td></tr>
							<?php endforeach; ?>
							<tr><td><span class="bold">Total</span></td><td><span class="bold"><?php echo $raids->zones_total['numQuests']; ?></span></td><td><div class="meter"><span style="width: <?php echo $raids->zones_total['questProgPct']; ?>%"></span></div></td><td class="tb-quest-prog-pct"><span class="bold"><?php echo $raids->zones_total['questProgPct']; ?>%</span></td></tr>
						</table>
					</div>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</body>
</html>