<?php include 'inc/include.php'; ?>
<head>
	<title>NPCs</title>
	<link rel="stylesheet" type="text/css" href="css/tracker.css">
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/tabs.js"></script>
</head>

<div id="t_wrapper">
	<?php include 'inc/menu.php'; ?>
	<?php include 'inc/precontents.php'; ?>
	<div id="content-wrapper">

<?php
	$npcTotal = new Npc;
	$npcTotal->getNpcs($list = true, 'entry LIKE "%'.@$_GET['q'].'%" OR name LIKE "%'.@$_GET['q'].'%"');
	if($npcTotal->numResults > 0)
	{
		$pages = new Paginator;
		$pages->items_total = $npcTotal->numResults;
		$pages->mid_range = 9;
		$pages->paginate();
	}else
	{
		die('<h1>No results found!</h1><p>No results could be found. Try another keyword!</p>');
	}
?>
<?php if(!isset($_GET['q'])): ?>
<h1>NPCs (<?php echo $npcTotal->numResults; ?>)</h1>
<?php else: ?>
<h1>NPCs matching '<?php echo $_GET['q']; ?>' (<?php echo $npcTotal->numResults; ?>)</h1>
<?php endif; ?>

	<?php
	$npc = new Npc;
	$npc->getNpcs($list = true, 'entry LIKE "%'.@$_GET['q'].'%" OR name LIKE "%'.@$_GET['q'].'%"', null, $pages->limit);?>
		
		<div id="pag_options">
			<div id="pag_left">
				Showing <span class="bold"><?php echo $pages->show_min_out_of; ?></span> - <span class="bold"><?php echo $pages->show_max_out_of; ?></span> of <span class="bold"><?php echo $npcTotal->numResults; ?></span></p>
			</div>
			<div id="pag_right">
				<div id="items_per_page"><?php echo $pages->display_items_per_page(); ?></div>
				<div id="jump_menu"><?php echo $pages->display_jump_menu(); ?></div>
			</div>
		</div>

<div class="paginate_pages"><?php echo $pages->display_pages(); ?></div>

<table id="entries">
<th><div><span>ID</span></div></th><th><div><span>NPC</span></div></th><th><div><span>Guids</span></div></th><th><div><span>Status</span></div></th><th><div><span>Reports</span></div></th>
<?php foreach($npc->npc as $row): ?>
	<?php
	$report = new Report;
	$npc = new Npc;
	$npc_guid = new Npc;
	$npc_guid->getGuids($row['entry']);
	$report->getReports(1, $row['entry'], NPC);
	$npc->getStatus($row['entry']);
	?>
	<tr>
		<td><?php echo $row['entry']; ?></td>
		<td><a href="npc.php?npc=<?php echo $row['entry']; ?>"><?php echo $row['name']; ?></a></td>
		<td><?php echo $npc_guid->numResults; ?></td>
		<td><?php echo $npc->status; ?></td>
		<td><?php echo $report->numResults; ?></td>
	</tr>
<?php endforeach; ?>
</table>
</div>
</div>