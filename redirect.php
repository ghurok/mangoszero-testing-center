<?php
	include 'inc/include.php';
	
	$redirect = new Redirect;
	$redirect->doRedirect($_GET['redirect'], 4);

?>
<html>
<head>
	<title>Redirecting...</title>
	<link rel="stylesheet" type="text/css" href="css/tracker.css">
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/tabs.js"></script>
</head>
<body>
<div id="t_wrapper">
	<?php include 'inc/menu.php'; ?>
	<?php include 'inc/precontents.php'; ?>
	<div id="content-wrapper">
		<div id="main-content">
			<div id="content-left">
				<?php if(isset($_GET['s'])): ?>
					<?php $redirect->success($_GET['s']); ?>
					<div id="success_box"><?php echo $redirect->success; ?></div>
				<?php endif; ?>

				<?php if(isset($_GET['e'])): ?>
					<?php $redirect->error($_GET['e']); ?>
					<div id="error_box"><?php echo $redirect->error; ?></div>
				<?php endif; ?>
			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>