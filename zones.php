<?php
include 'inc/include.php';

$kalimdor = new Area;
$easternKingdoms = new Area;
$cities = new Area;

$phase = new Phase;
$quest = new Quest;

$kalimdor->getZoneInfoByZones($continents['Kalimdor']);
$easternKingdoms->getZoneInfoByZones($continents['Eastern Kingdoms']);
$cities->getZoneInfoByZones(null, $cities->getCitiesZoneId());

?>
<!DOCTYPE html>
<html>
<head>
	<title>Zones</title>
	<link rel="stylesheet" type="text/css" href="css/tracker.css">
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/tabs.js"></script>
</head>
<body>
	<div id="t_wrapper">
		<?php include 'inc/menu.php'; ?>
		<?php include 'inc/precontents.php'; ?>
		<div id="content-wrapper">
			<h1>Zones</h1>
			<div id="related">
				<div id="tabs-container">
					<ul>
						<li><a href="#1" class="showSingle" data-target="1">Kalimdor</a></li>
						<li><a href="#2" class="showSingle" data-target="2">Eastern Kingdoms</a></li>
						<li><a href="#3" class="showSingle" data-target="3">Cities</a></li>
					</ul>
					<div class="clear"></div>
				</div>
				<div id="tabs-content">
					<div id="tabs-content-1" class="targetDiv">
						<table>
							<th><div><span>Zone</span></div></th><th><div><span>Sub-Zones</span></div></th><th><div><span>Quests</span></div></th><th colspan="2"><div><span>Quest Progress (Tested)</span></div></th>
							<?php foreach($kalimdor->zones_list as $row): ?>
								<tr><td><a href="zone.php?zone=<?php echo $row['id']; ?>"><?php echo $row['zone_name']; ?></a></td><td><?php echo $row['numSubZones']; ?></td><td><?php echo $row['numQuests']; ?></td><td><div class="meter"><span style="width: <?php echo $row['questProgPct']; ?>%"></span></div></td><td class="tb-quest-prog-pct"><?php echo $row['questProgPct']; ?>%</td></tr>
							<?php endforeach; ?>
							<tr><td><span class="bold">Total</span></td><td><span class="bold"><?php echo $kalimdor->zones_total['numSubZones']; ?></span></td><td><span class="bold"><?php echo $kalimdor->zones_total['numQuests']; ?></span></td><td><div class="meter"><span style="width: <?php echo $kalimdor->zones_total['questProgPct']; ?>%"></span></div></td><td class="tb-quest-prog-pct"><span class="bold"><?php echo $kalimdor->zones_total['questProgPct']; ?>%</span></td></tr>
						</table>
					</div>
					<div id="tabs-content-2" class="targetDiv">
						<table>
							<th><div><span>Zone</span></div></th><th><div><span>Sub-Zones</span></div></th><th><div><span>Quests</span></div></th><th colspan="2"><div><span>Quest Progress (Tested)</span></div></th>
							<?php foreach($easternKingdoms->zones_list as $row): ?>
								<tr><td><a href="zone.php?zone=<?php echo $row['id']; ?>"><?php echo $row['zone_name']; ?></a></td><td><?php echo $row['numSubZones']; ?></td><td><?php echo $row['numQuests']; ?></td><td><div class="meter"><span style="width: <?php echo $row['questProgPct']; ?>%"></span></div></td><td class="tb-quest-prog-pct"><?php echo $row['questProgPct']; ?>%</td></tr>
							<?php endforeach; ?>
							<tr><td><span class="bold">Total</span></td><td><span class="bold"><?php echo $easternKingdoms->zones_total['numSubZones']; ?></span></td><td><span class="bold"><?php echo $easternKingdoms->zones_total['numQuests']; ?></span></td><td><div class="meter"><span style="width: <?php echo $easternKingdoms->zones_total['questProgPct']; ?>%"></span></div></td><td class="tb-quest-prog-pct"><span class="bold"><?php echo $easternKingdoms->zones_total['questProgPct']; ?>%</span></td></tr>
						</table>
					</div>
					<div id="tabs-content-3" class="targetDiv">
						<table>
							<th><div><span>Zone</span></div></th><th><div><span>Sub-Zones</span></div></th><th><div><span>Quests</span></div></th><th colspan="2"><div><span>Quest Progress (Tested)</span></div></th>
							<?php foreach($cities->zones_list as $row): ?>
								<tr><td><a href="zone.php?zone=<?php echo $row['id']; ?>"><?php echo $row['zone_name']; ?></a></td><td><?php echo $row['numSubZones']; ?></td><td><?php echo $row['numQuests']; ?></td><td><div class="meter"><span style="width: <?php echo $row['questProgPct']; ?>%"></span></div></td><td class="tb-quest-prog-pct"><?php echo $row['questProgPct']; ?>%</td></tr>
							<?php endforeach; ?>
							<tr><td><span class="bold">Total</span></td><td><span class="bold"><?php echo $cities->zones_total['numSubZones']; ?></span></td><td><span class="bold"><?php echo $cities->zones_total['numQuests']; ?></span></td><td><div class="meter"><span style="width: <?php echo $cities->zones_total['questProgPct']; ?>%"></span></div></td><td class="tb-quest-prog-pct"><span class="bold"><?php echo $cities->zones_total['questProgPct']; ?>%</span></td></tr>
						</table>
					</div>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</body>
</html>