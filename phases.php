<?php
include 'inc/include.php';

$phase1 = new Area;
$phase2 = new Area;
$phase3 = new Area;

$phase = new Phase;
$quest = new Quest;

$phase1->getZoneInfoByZones(null, $phase->getPhase1Zones());
$phase2->getZoneInfoByZones(null, $phase->getPhase2Zones());
$phase3->getZoneInfoByZones(null, $phase->getPhase3Zones());

?>
<!DOCTYPE html>
<html>
<head>
	<title>Testing Phases</title>
	<link rel="stylesheet" type="text/css" href="css/tracker.css">
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/tabs.js"></script>
</head>
<body>
	<div id="t_wrapper">
		<?php include 'inc/menu.php'; ?>
		<?php include 'inc/precontents.php'; ?>
		<div id="content-wrapper">
			<div id="main-content">
				<section id="quickfacts">
					<h2>Current Phase</h2>
						Phase 1
				</section>
				<h1>Testing Phases</h1>
				<p>
					We have divided the testing of the game content into different phases. We will limit every phase to certain zones.
					<br /> Each phase will be considered complete when ALL quests in the phase have been tested.
					<br /> When the above requirement is completed we will switch phase and unlock new zones.
					<br /><br /> <a href="">Read more in our FAQ about phases.</a>
				</p>
			</div>
			<div id="related">
				<div id="tabs-container">
					<ul>
						<li><a href="#1" class="showSingle" data-target="1">Phase 1</a></li>
						<li><a href="#2" class="showSingle" data-target="2">Phase 2</a></li>
						<li><a href="#3" class="showSingle" data-target="3">Phase 3</a></li>
						<li><a href="#4" class="showSingle" data-target="4">Phase 4</a></li>
						<li><a href="#5" class="showSingle" data-target="5">Phase 5</a></li>
						<li><a href="#6" class="showSingle" data-target="6">Phase 6</a></li>
						<li><a href="#7" class="showSingle" data-target="7">...</a></li>
					</ul>
					<div class="clear"></div>
				</div>
				<div id="tabs-content">
					<div id="tabs-content-1" class="targetDiv">
						<table>
							<th><div><span>Zone</span></div></th><th><div><span>Sub-Zones</span></div></th><th><div><span>Quests</span></div></th><th colspan="2"><div><span>Quest Progress (Tested)</span></div></th>
							<?php foreach($phase1->zones_list as $row): ?>
								<tr><td><a href="zone.php?zone=<?php echo $row['id']; ?>"><?php echo $row['zone_name']; ?></a></td><td><?php echo $row['numSubZones']; ?></td><td><?php echo $row['numQuests']; ?></td><td><div class="meter"><span style="width: <?php echo $row['questProgPct']; ?>%"></span></div></td><td class="tb-quest-prog-pct"><?php echo $row['questProgPct']; ?>%</td></tr>
							<?php endforeach; ?>
							<tr><td><span class="bold">Total</span></td><td><span class="bold"><?php echo $phase1->zones_total['numSubZones']; ?></span></td><td><span class="bold"><?php echo $phase1->zones_total['numQuests']; ?></span></td><td><div class="meter"><span style="width: <?php echo $phase1->zones_total['questProgPct']; ?>%"></span></div></td><td class="tb-quest-prog-pct"><span class="bold"><?php echo $phase1->zones_total['questProgPct']; ?>%</span></td></tr>
						</table>
					</div>
					<div id="tabs-content-2" class="targetDiv">
						<table>
							<th><div><span>Zone</span></div></th><th><div><span>Sub-Zones</span></div></th><th><div><span>Quests</span></div></th><th colspan="2"><div><span>Quest Progress (Tested)</span></div></th>
							<?php foreach($phase2->zones_list as $row): ?>
								<tr><td><a href="zone.php?zone=<?php echo $row['id']; ?>"><?php echo $row['zone_name']; ?></a></td><td><?php echo $row['numSubZones']; ?></td><td><?php echo $row['numQuests']; ?></td><td><div class="meter"><span style="width: <?php echo $row['questProgPct']; ?>%"></span></div></td><td class="tb-quest-prog-pct"><?php echo $row['questProgPct']; ?>%</td></tr>
							<?php endforeach; ?>
							<tr><td><span class="bold">Total</span></td><td><span class="bold"><?php echo $phase2->zones_total['numSubZones']; ?></span></td><td><span class="bold"><?php echo $phase2->zones_total['numQuests']; ?></span></td><td><div class="meter"><span style="width: <?php echo $phase2->zones_total['questProgPct']; ?>%"></span></div></td><td class="tb-quest-prog-pct"><span class="bold"><?php echo $phase2->zones_total['questProgPct']; ?>%</span></td></tr>
						</table>
					</div>
					<div id="tabs-content-3" class="targetDiv">
						<table>
							<th><div><span>Zone</span></div></th><th><div><span>Sub-Zones</span></div></th><th><div><span>Quests</span></div></th><th colspan="3"><div><span>Quest Progress (Tested)</span></div></th>
							<?php foreach($phase3->zones_list as $row): ?>
								<tr><td><a href="zone.php?zone=<?php echo $row['id']; ?>"><?php echo $row['zone_name']; ?></a></td><td><?php echo $row['numSubZones']; ?></td><td><?php echo $row['numQuests']; ?></td><td><div class="meter"><span style="width: <?php echo $row['questProgPct']; ?>%"></span></div></td><td class="tb-quest-prog-pct"><?php echo $row['questProgPct']; ?>%</td></tr>
							<?php endforeach; ?>
							<tr><td><span class="bold">Total</span></td><td><span class="bold"><?php echo $phase3->zones_total['numSubZones']; ?></span></td><td><span class="bold"><?php echo $phase3->zones_total['numQuests']; ?></span></td><td><div class="meter"><span style="width: <?php echo $phase3->zones_total['questProgPct']; ?>%"></span></div></td><td class="tb-quest-prog-pct"><span class="bold"><?php echo $phase3->zones_total['questProgPct']; ?>%</span></td></tr>
						</table>
					</div>
					<div id="tabs-content-7" class="targetDiv">
						<p>We might add more phases later if necessary.</p>
					</div>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</body>
</html>