<form id="search_form" method="post">
	<input type="text" name="search_string">
	<select name="search_type">
		<option value="<?php echo QUEST; ?>">Quests</option>
		<option value="<?php echo ITEM; ?>">Items</option>
		<option value="<?php echo NPC; ?>">NPCs</option>
		<option value="<?php echo _OBJECT; ?>">Objects</option>
		<option value="<?php echo SPELL; ?>">Spells</option>
	</select>
	<input type="submit" name="search" value="Search">
</form>