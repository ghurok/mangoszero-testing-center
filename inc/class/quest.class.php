<?php
class Quest
{
	public $quest;
	public $status;
	public $numResults;
	public $status_raw;

	public function getQuests($rows_sel = false, $data = null, $where = null, $join = null, $limit = null)
	{
		$faction = new Faction;
		$money = new Money;

		if($data['zoneorsort'] !== null)
			$where = '`zoneorsort`='.implode(' OR `zoneorsort`=', $data['zoneorsort']);

		if($data['RequiredClasses'] !== null)
			$where .= ' OR `RequiredClasses`='.implode(' OR `RequiredClasses`=', $data['RequiredClasses']);

		switch($rows_sel)
		{
			case 1:
				$rows = '`entry`, `Title`';
			break;
			case 2:
				$rows = '`entry`, `MinLevel`, `QuestLevel`, `Type`, `RequiredRaces`, `QuestFlags`,
					 	`PrevQuestId`, `NextQuestId`, `NextQuestInChain`, `Title`, `Details`,
					 	`Objectives`, `OfferRewardText`, `RequestItemsText`, `EndText`, `RewMoneyMaxLevel`,
					 	`RewRepFaction1`, `RewRepFaction2`, `RewRepFaction3`, `RewRepFaction4`, `RewRepFaction5`,
					 	`RewRepValue1`, `RewRepValue2`, `RewRepValue3`, `RewRepValue4`, `RewRepValue5`,
					 	RewOrReqMoney, RewMoneyMaxLevel';
			break;
			case 3:
				$rows = '`id`';
			break;
		}

		$db = new Database('dbh');
		$db->select('dbh','quest_template', $rows, ''.$where.'', null, ''.$join.'', $limit);
		$result = $db->getResult();
		$numResults = $db->numResults;
		if($numResults >= 1)
		{
			if($rows_sel == 2)
			{
				for($i = 0; $i < count($result); $i++)
				{
					$result[$i]['RequiredSide'] = $this->getSide($result[$i]['RequiredRaces']);
					$result[$i]['Sharable'] = $this->isSharable($result[$i]['QuestFlags']);
					$result[$i]['Type'] = $this->getType($result[$i]['Type']);
					$result[$i]['Objectives'] = $this->formatQuestText($result[$i]['Objectives']);
					$result[$i]['Details'] = $this->formatQuestText($result[$i]['Details']);
					$result[$i]['RequestItemsText'] = $this->formatQuestText($result[$i]['RequestItemsText']);
					$result[$i]['OfferRewardText'] = $this->formatQuestText($result[$i]['OfferRewardText']);
					$result[$i]['QuestStart'] = $this->getQuestStart($result[$i]['entry']);
					$result[$i]['QuestEnd'] = $this->getQuestEnd($result[$i]['entry']);

					// reputation
					$factions_id = array($result[$i]['RewRepFaction1'], $result[$i]['RewRepFaction2'], $result[$i]['RewRepFaction3'], $result[$i]['RewRepFaction4'], $result[$i]['RewRepFaction5']);

					// check if any factions are specified in the database
					for($f = 0; $f < count($factions_id); $f++)
					{
						if($factions_id[$f] > 0)
							$repIsGained = true;
					}

					if($repIsGained)
					{
						$faction->getFactions($factions_id);

						for($f = 0; $f < count($faction->factions); $f++)
						{
							for($n = 0; $n < count($factions_id); $n++)
							{
								$a = $n+1;
								if($faction->factions[$f]['id'] == $result[$i]['RewRepFaction'.$a.''])
									$result[$i]['Reputation'][] = array('id' => $faction->factions[$f]['id'], 'faction' => $faction->factions[$f]['name'], 'reputation' => $result[$i]['RewRepValue'.$a.'']);
							}
						}
					}else
					{
						$result[$i]['Reputation'] = NULL;
					}

					// maybe improve later: http://www.wowwiki.com/Experience_point
					if($result[$i]['RewMoneyMaxLevel'] > 0)
						$result[$i]['XP_Reward'] = $result[$i]['RewMoneyMaxLevel'] / 0.6;
					else
						$result[$i]['XP_Reward'] = 0;

					// money
					$result[$i]['RewOrReqMoney'] = $money->getFormatMoney($result[$i]['RewOrReqMoney']);
					
					if($result[$i]['RewMoneyMaxLevel'] > 0)
						$result[$i]['RewMoneyMaxLevel'] = $money->getFormatMoney($result[$i]['RewMoneyMaxLevel']);
					
					$result[$i]['PrevQuest'] = $this->getPrevQuest($result[$i]['PrevQuestId']);
					
					$result[$i]['NextQuest'] = $this->getNextQuest($result[$i]['NextQuestId']);
					
					if($result[$i]['NextQuestInChain'] > 0 && $result[$i]['NextQuestId'] == 0)
						$result[$i]['NextQuest'] = $this->getNextQuest($result[$i]['NextQuestInChain']);
				}
			}
			$this->numResults = $numResults;
			return $this->quest = $result;
		}else
		{
			return false;
		}
	}

	public function getStatus($entry = false, $text = false)
	{
		$db = new Database('dbh1');
		$db->select('dbh1','testing_entry_status', 'entry, status', '(`entry`='.$entry.') AND `type`='.QUEST);
		$result = $db->getResult();
		$numResults = $db->numResults;

		switch(@$result[0]['status'])
		{
			case QUEST_BUGS_FOUND:
				if(@$text)
					$status = BUGS_FOUND;
				else
					$status = '<span class="tag tag2">'.BUGS_FOUND.'</span>';
			break;
			case QUEST_NOT_COMPLETABLE:
				if(@$text)
					$status = NOT_COMPLETABLE;
				else
					$status = '<span class="tag tag2">'.NOT_COMPLETABLE.'</span>';
			break;
			case QUEST_WORKING_PERFECTLY:
				if(@$text)
					$status = WORKING_PERFECTLY;
				else
					$status = '<span class="tag tag8">'.WORKING_PERFECTLY.'</span>';
			break;
			case QUEST_COMPLETABLE_NO_BUGS_FOUND:
				if(@$text)
					$status = COMPLETABLE_NO_BUGS_FOUND;
				else
					$status = '<span class="tag tag4">'.COMPLETABLE_NO_BUGS_FOUND.'</span>';
			break;
			case QUEST_OBSOLETE:
				if(@$text)
					$status = OBSOLETE;
				else
					$status = '<span class="tag tag2">'.OBSOLETE.'</span>';
			break;
			case NEEDS_MORE_TESTING:
				if(@$text)
					$status = NEEDS_MORE_TESTING_TEXT;
				else
					$status = '<span class="tag tag_nmt">'.NEEDS_MORE_TESTING_TEXT.'</span>';
			break;
			default:
				if(@$text)
					$status = UNTESTED_TEXT;
				else
					$status = '<span class="tag tag0">'.UNTESTED_TEXT.'</span>';
		}
		
		return $this->status = $status;
	}

	public function getSide($RequiredRaces)
	{
		switch($RequiredRaces)
		{
			case 0:
				return 'Both';
			break;
			case 77:
				return '<span class="icon-alliance">Alliance</span>';
			break;
			case 178:
				return '<span class="icon-horde">Horde</span>';
			break;
			default:
				return false;
		}
	}

	public function isSharable($QuestFlags)
	{
		$bin = decbin($QuestFlags);
		$bin = substr("00000000000000",0,14 - strlen($bin)).$bin;

		if($bin[10] == 1)
			return 'Sharable';
		else
			return 'Not Sharable';
	}

	public function getType($Type)
	{
		switch($Type)
		{
			case 1:
				return 'Elite';
			break;
			case 21:
				return 'Life';
			break;
			case 41:
				return 'PvP';
			break;
			case 62:
				return 'Raid';
			break;
			case 81:
				return 'Dungeon';
			break;
			case 82:
				return 'World Event';
			break;
			case 83:
				return 'Legendary';
			break;
			default:
				return false;
		}
	}

	public function formatQuestText($text)
	{
		$filter = array('$B', '$C', '$c', '$R', '$r', '$N', '$n');
		$replace = array('<br />', '&lt;Class&gt;', '&lt;class&gt;', '&lt;Race&gt;', '&lt;race&gt;', '&lt;Name&gt;', '&lt;name&gt;');
		$text = str_replace($filter, $replace, $text);

		return $text;
	}

	public function getQuestStart($quest)
	{
		$db = new Database('dbh');
		$db->select('dbh','creature_questrelation', '*', 'quest='.$quest, null, 'creature_template ON creature_questrelation.id=creature_template.entry');
		$result = $db->getResult();
		$numResults = $db->numResults;
		if($numResults > 0)
		{
			return '<a href="npc.php?npc='.$result[0]['entry'].'">'.$result[0]['name'].'</a>';
		}else
		{
			$db = new Database('dbh');
			$db->select('dbh','gameobject_questrelation', '*', 'quest='.$quest, null, 'gameobject_template ON gameobject_questrelation.id=gameobject_template.entry');
			$result = $db->getResult();
			$numResults = $db->numResults;
			if($numResults > 0)
			{
				return '<a href="object.php?object='.$result[0]['entry'].'">'.$result[0]['name'].'</a>';
			}
		}
	}

	public function getQuestEnd($quest)
	{
		$db = new Database('dbh');
		$db->select('dbh','creature_involvedrelation', '*', 'quest='.$quest, null, 'creature_template ON creature_involvedrelation.id=creature_template.entry');
		$result = $db->getResult();
		$numResults = $db->numResults;
		if($numResults > 0)
		{
			return '<a href="npc.php?npc='.$result[0]['entry'].'">'.$result[0]['name'].'</a>';
		}else
		{
			$db = new Database('dbh');
			$db->select('dbh','gameobject_involvedrelation', '*', 'quest='.$quest, null, 'gameobject_template ON gameobject_involvedrelation.id=gameobject_template.entry');
			$result = $db->getResult();
			$numResults = $db->numResults;
			if($numResults > 0)
			{
				return '<a href="object.php?object='.$result[0]['entry'].'">'.$result[0]['name'].'</a>';
			}
		}
	}

	public function getPrevQuest($quest)
	{
		$db = new Database('dbh');
		$db->select('dbh','quest_template', 'entry, Title', 'entry='.$quest);
		$result = $db->getResult();
		$numResults = $db->numResults;
		if($numResults > 0)
			return '<a href="quest.php?quest='.$result[0]['entry'].'">'.$result[0]['Title'].'</a>';
		else
			return false;
	}

	public function getNextQuest($quest)
	{
		$db = new Database('dbh');
		$db->select('dbh','quest_template', 'entry, Title', 'entry='.$quest);
		$result = $db->getResult();
		$numResults = $db->numResults;
		if($numResults > 0)
			return '<a href="quest.php?quest='.$result[0]['entry'].'">'.$result[0]['Title'].'</a>';
		else
			return false;
	}

	public function getQuestWebLinks($data)
	{
		// Wayback Machine: www.goblinworkshop.com
		$data['WB_GoblinWorkshop'] = '<a href="http://web.archive.org/web/*/http://www.goblinworkshop.com/quests/'.str_replace(array(' ', '\''), array('-', ''), strtolower($data['Title'])).'.html" target="_blank">Wayback Machine: Goblin Workshop</a>';
		$data['WB_Thottbot'] = '<a href="http://web.archive.org/web/*/http://thottbot.com/q'.$data['entry'].'" target="_blank">Wayback Machine: Thottbot</a>';
		$data['WB_Allakhazam'] = '<a href="http://web.archive.org/web/*/http://wow.allakhazam.com/db/quest.html?wquest='.$data['entry'].'" target="_blank">Wayback Machine: Allakhazam</a>';
		$data['Old_Wowhead'] = '<a href="http://old.wowhead.com/quest='.$data['entry'].'" target="_blank">Wowhead (WoTLK)</a>';
		$data['Wowd'] = '<a href="http://wowd.org/quests/'.$data['entry'].'.html" target="_blank">Wowd</a>';
		$data['Wowpedia'] = '<a href="http://wowpedia.org/'.$data['Title'].'" target="_blank">Wowpedia</a>';

		$this->quest['WebLinks'] = $data;
	}
}

?>