<?php

class Spell
{
	public $spell;
	public $status;
	public $numResults;

	public function getSpells($list = false, $where = null, $join = null, $limit = null)
	{
		if($list == true)
			$rows = '`entry`, `COL 118`, `name`, `rank`';
		else
			$rows = '`entry`, `COL 118`, `name`, `rank`';

		$db = new Database('dbh1');
		$db->select('dbh1','spells', $rows, ''.$where.'', null, ''.$join.'', $limit);
		$result = $db->getResult();
		$numResults = $db->numResults;
		if($numResults >= 1)
		{
			$this->numResults = $numResults;
			return $this->spell = $result;
		}else
		{
			return false;
		}
	}

	public function getStatus($entry = false)
	{
		$db = new Database('dbh1');
		$db->select('dbh1','testing_entry_status', 'entry, status', '(`entry`='.$entry.') AND `type`='.SPELL);
		$result = $db->getResult();
		$numResults = $db->numResults;

		switch(@$result[0]['status'])
		{
			case SPELL_NOT_WORKING_AS_INTENDED:
				if(@$text)
					$status = NOT_WORKING_AS_INTENDED;
				else
					$status = '<span class="tag tag2">'.NOT_WORKING_AS_INTENDED.'</span>';
			break;
			case SPELL_OBSOLETE:
				if(@$text)
					$status = OBSOLETE;
				else
					$status = '<span class="tag tag2">'.OBSOLETE.'</span>';
			break;
			case SPELL_NO_BUGS_FOUND:
				if(@$text)
					$status = NO_BUGS_FOUND;
				else
					$status = '<span class="tag tag8">'.NO_BUGS_FOUND.'</span>';
			break;
			case NEEDS_MORE_TESTING:
				if(@$text)
					$status = NEEDS_MORE_TESTING_TEXT;
				else
					$status = '<span class="tag tag_nmt">'.NEEDS_MORE_TESTING_TEXT.'</span>';
			break;
			default:
				if(@$text)
					$status = UNTESTED_TEXT;
				else
					$status = '<span class="tag tag0">'.UNTESTED_TEXT.'</span>';
		}

		return $this->status = $status;

		/*$report = new Report;
		$report->getReports(null, $entry, SPELL);

		if($report->numResults > 0)
		{
			$SPELL_NOT_WORKING_AS_INTENDED = 0;
			$SPELL_NO_BUGS_FOUND = 0;
			$SPELL_OBSOLETE = 0;

			foreach($report->report as $report):
				switch($report['status'])
				{
					case SPELL_NOT_WORKING_AS_INTENDED:
						$SPELL_NOT_WORKING_AS_INTENDED++;
					break;
					case SPELL_OBSOLETE:
						$SPELL_OBSOLETE++;
					break;
					case SPELL_NO_BUGS_FOUND:
						$SPELL_NO_BUGS_FOUND++;
					break;
				}
			endforeach;

			if(@$SPELL_NOT_WORKING_AS_INTENDED >= SPELL_NOT_WORKING_AS_INTENDED)
				return $this->status = '<span class="tag tag2">'.NOT_WORKING_AS_INTENDED.'</span>';
			elseif(@$SPELL_NO_BUGS_FOUND >= SPELL_NO_BUGS_FOUND)
				return $this->status = '<span class="tag tag4">'.NO_BUGS_FOUND.'</span>';
			elseif(@$SPELL_OBSOLETE >= SPELL_OBSOLETE)
				return $this->status = '<span class="tag tag2">'.OBSOLETE.'</span>';
			elseif(@$SPELL_NOT_WORKING_AS_INTENDED > 0 || @$SPELL_NO_BUGS_FOUND > 0  || @$SPELL_OBSOLETE > 0)
				return $this->status = '<span class="tag tag_nmt">'.NEEDS_MORE_TESTING.'</span>';
			else
				return $this->status = '<span class="tag tag0">'.UNTESTED.'</span>';
		}else
		{
			return $this->status = '<span class="tag tag0">'.UNTESTED.'</span>';
		}*/
	}

	public function getIcon($displayid)
	{
		$db = new Database('dbh1');
		$db->select('dbh1','spellicons', '*', '`COL 1`='.$displayid);
		$result = $db->getResult();
		$numResults = $db->numResults;
		if($numResults > 0)
		{
			$result[0]['COL 2'] = substr($result[0]['COL 2'], 14);
			return 'img/icons/small/'.$result[0]['COL 2'].'.png';
		}
	}
}

?>