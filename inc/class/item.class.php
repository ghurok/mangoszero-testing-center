<?php

class Item
{
	public $item;
	public $status;
	public $numResults;

	public function getItems($list = false, $where = null, $join = null, $limit = null)
	{
		if($list == true)
			$rows = 'entry, name, displayid, Quality';
		else
			$rows = 'entry, class, name, displayid, Quality, Flags, BuyPrice,
					 SellPrice, ItemLevel, RequiredLevel, bonding';

		$db = new Database('dbh');
		$db->select('dbh', 'item_template', $rows, ''.$where.'', null, ''.$join.'', $limit);
		$result = $db->getResult();
		$numResults = $db->numResults;
		if($numResults >= 1)
		{
			for($i = 0; $i < count($result); $i++)
			{
				if($list == false)
				{
					$result[$i]['bonding'] = $this->getBonding($result[$i]['bonding']);
					$result[$i]['class'] = $this->getClass($result[$i]['class']);
				}

				switch ($result[$i]['Quality'])
				{
					case 0:
						$result[$i]['item_color'] = '#9d9d9d';
					break;
					case 1:
						$result[$i]['item_color'] = '#ffffff';
					break;
					case 2:
						$result[$i]['item_color'] = '#1eff00';
					break;
					case 3:
						$result[$i]['item_color'] = '#0070ff';
					break;
					case 4:
						$result[$i]['item_color'] = '#a335ee';
					break;
					case 5:
						$result[$i]['item_color'] = '#ff8000';
					break;
					case 6:
						$result[$i]['item_color'] = 'red';
					break;
					default:
						$result[$i]['item_color'] = '#ffffff';
				}
			}
			$this->numResults = $numResults;
			return $this->item = $result;
		}else
		{
			return false;
		}
	}

	public function getStatus($entry = false)
	{
		$db = new Database('dbh1');
		$db->select('dbh1','testing_entry_status', 'entry, status', '(`entry`='.$entry.') AND `type`='.ITEM);
		$result = $db->getResult();
		$numResults = $db->numResults;

		switch(@$result[0]['status'])
		{
			case ITEM_NOT_WORKING_AS_INTENDED:
				if(@$text)
					$status = NOT_WORKING_AS_INTENDED;
				else
					$status = '<span class="tag tag2">'.NOT_WORKING_AS_INTENDED.'</span>';
			break;
			case ITEM_OBSOLETE:
				if(@$text)
					$status = OBSOLETE;
				else
					$status = '<span class="tag tag2">'.OBSOLETE.'</span>';
			break;
			case ITEM_WORKING_PERFECTLY:
				if(@$text)
					$status = WORKING_PERFECTLY;
				else
					$status = '<span class="tag tag8">'.WORKING_PERFECTLY.'</span>';
			break;
			case ITEM_NO_BUGS_FOUND:
				if(@$text)
					$status = NO_BUGS_FOUND;
				else
					$status = '<span class="tag tag4">'.NO_BUGS_FOUND.'</span>';
			break;
			case NEEDS_MORE_TESTING:
				if(@$text)
					$status = NEEDS_MORE_TESTING_TEXT;
				else
					$status = '<span class="tag tag_nmt">'.NEEDS_MORE_TESTING_TEXT.'</span>';
			break;
			default:
				if(@$text)
					$status = UNTESTED_TEXT;
				else
					$status = '<span class="tag tag0">'.UNTESTED_TEXT.'</span>';
		}

		return $this->status = $status;


		/*$report = new Report;
		$report->getReports(null, $entry, ITEM);

		if($report->numResults > 0)
		{
			$ITEM_NOT_WORKING_AS_INTENDED = 0;
			$ITEM_NO_BUGS_FOUND = 0;
			$ITEM_WORKING_PERFECTLY = 0;
			$ITEM_OBSOLETE = 0;

			foreach($report->report as $report):
				switch($report['status'])
				{
					case ITEM_NOT_WORKING_AS_INTENDED:
						$ITEM_NOT_WORKING_AS_INTENDED++;
					break;
					case ITEM_OBSOLETE:
						$ITEM_OBSOLETE++;
					break;
					case ITEM_NO_BUGS_FOUND:
						$ITEM_NO_BUGS_FOUND++;
					break;
					case ITEM_WORKING_PERFECTLY:
						$ITEM_WORKING_PERFECTLY++;
					break;
				}
			endforeach;

			if(@$ITEM_NOT_WORKING_AS_INTENDED >= ITEM_NOT_WORKING_AS_INTENDED_MIN)
				return $this->status = '<span class="tag tag2">'.NOT_WORKING_AS_INTENDED.'</span>';
			elseif(@$ITEM_WORKING_PERFECTLY >= ITEM_WORKING_PERFECTLY_MIN)
				return $this->status = '<span class="tag tag8">'.WORKING_PERFECTLY.'</span>';
			elseif(@$ITEM_NO_BUGS_FOUND >= ITEM_NO_BUGS_FOUND_MIN)
				return $this->status = '<span class="tag tag4">'.NO_BUGS_FOUND.'</span>';
			elseif(@$ITEM_OBSOLETE >= ITEM_OBSOLETE_MIN)
				return $this->status = '<span class="tag tag1">'.OBSOLETE.'</span>';
			elseif(@$ITEM_NOT_WORKING_AS_INTENDED > 0 || @$ITEM_NO_BUGS_FOUND > 0  || @$ITEM_WORKING_PERFECTLY > 0 || @$ITEM_OBSOLETE > 0)
				return $this->status = '<span class="tag tag_nmt">'.NEEDS_MORE_TESTING.'</span>';
			else
				return $this->status = '<span class="tag tag0">'.UNTESTED.'</span>';
		}else
		{
			return $this->status = '<span class="tag tag0">'.UNTESTED.'</span>';
		}*/
	}

	public function getBonding($bonding)
	{
		switch($bonding)
		{
			case 1:
				return 'Binds when picked up';
			break;
			case 2:
				return 'Binds when equipped';
			break;
			case 3:
				return 'Binds when used';
			break;
			case 4:
				return 'Quest item';
			break;
			default:
				return 'None';
		}
	}

	public function getClass($class)
	{
		switch($class)
		{
			case 0:
				return 'Consumable';
			break;
			case 1:
				return 'Container';
			break;
			case 2:
				return 'Weapon';
			break;
			case 3:
				return 'Gem';
			break;
			case 4:
				return 'Armor';
			break;
			case 5:
				return 'Reagent';
			break;
			case 6:
				return 'Projectile';
			break;
			case 7:
				return 'Trade Goods';
			break;
			case 8:
				return 'Generic(OBSOLETE)';
			break;
			case 9:
				return 'Recipe';
			break;
			case 10:
				return 'Money(OBSOLETE)';
			break;
			case 11:
				return 'Quiver';
			break;
			case 12:
				return 'Quest';
			break;
			case 13:
				return 'Key';
			break;
			case 14:
				return 'Permanent(OBSOLETE)';
			break;
			case 15:
				return 'Miscellaneous';
			break;
			default:
				return 'UNKNOWN!!';
		}
	}

	public function getIcon($displayid)
	{
		$db = new Database('dbh1');
		$db->select('dbh1','itemicon', 'name', 'id='.$displayid);
		$result = $db->getResult();
		$numResults = $db->numResults;
		if($numResults > 0)
		{
			return 'img/icons/small/'.$result[0]['name'].'.png';
		}
	}
}

?>