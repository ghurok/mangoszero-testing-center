<?php
class Date
{
	public function getDate()
	{
		return date('Y-m-d');
	}

	public function getYear()
	{
		return date('Y');
	}
}
?>