<?php
class QuestSort
{
	public $sort_list;
	public $sort_total;

	public $sort;

	// returned by function getProfQuestSortIdFromProfId
	public $profession_sort;

	// returned by function getClassQuestSortIdFromClassId
	public $class_sort;

	// returned by function getMiscQuestSortIdFromMiscId
	public $misc_sort;

	public function getClassId($raw = true)
	{
		$array = array(9, 7, 8, 3, 11, 1, 2, 4, 5);

		if($raw == true)
		{
			return $array;
		}else
		{
			for($i = 0; $i < count($array); $i++)
			{
				$classes[$i]['class_id'] = $array[$i];
				switch($array[$i])
				{
					case 1:
						$classes[$i]['RequiredClasses'] = 1;
					break;
					case 2:
						$classes[$i]['RequiredClasses'] = 2;
					break;
					case 3:
						$classes[$i]['RequiredClasses'] = 4;
					break;
					case 4:
						$classes[$i]['RequiredClasses'] = 8;
					break;
					case 5:
						$classes[$i]['RequiredClasses'] = 16;
					break;
					case 7:
						$classes[$i]['RequiredClasses'] = 64;
					break;
					case 8:
						$classes[$i]['RequiredClasses'] = 128;
					break;
					case 9:
						$classes[$i]['RequiredClasses'] = 256;
					break;
					case 11:
						$classes[$i]['RequiredClasses'] = 1024;
					break;
				}
			}
			return $classes;
		}
	}

	public function getClassQuestSortId()
	{
		// Warlock, Shaman, Mage, Hunter, Druid, Warrior, Paladin, Rogue, Priest
		return array(-61, -82, -161, -261, -263, -81, -141, -162, -262);
	}

	public function getClassQuestSortIdFromClassId($class_id)
	{
		$array = $this->getClassQuestSortId();
		
		switch($class_id)
		{
			case 9:
				return $this->class_sort = array($array[0]);
			break;
			case 7:
				return $this->class_sort = array($array[1]);
			break;
			case 8:
				return $this->class_sort = array($array[2]);
			break;
			case 3:
				return $this->class_sort = array($array[3]);
			break;
			case 11:
				return $this->class_sort = array($array[4]);
			break;
			case 1:
				return $this->class_sort = array($array[5]);
			break;
			case 2:
				return $this->class_sort = array($array[6]);
			break;
			case 4:
				return $this->class_sort = array($array[7]);
			break;
			case 5:
				return $this->class_sort = array($array[8]);
			break;
			default:
				return $this->class_sort = false;
		}
	}

	public function getProfQuestSortId()
	{
		return array(-24, -101, -121, -181, -182, -201, -264, -304, -324);
	}

	public function getProfQuestSortIdFromProfId($skill_id)
	{

		$array = $this->getProfQuestSortId();

		switch($skill_id)
		{
			case 182:
				return $this->profession_sort = array($array[0]);
			break;
			case 356:
				return $this->profession_sort = array($array[1]);
			break;
			case 164:
				return $this->profession_sort = array($array[2]);
			break;
			case 171:
				return $this->profession_sort = array($array[3]);
			break;
			case 165:
				return $this->profession_sort = array($array[4]);
			break;
			case 202:
				return $this->profession_sort = array($array[5]);
			break;
			case 197:
				return $this->profession_sort = array($array[6]);
			break;
			case 185:
				return $this->profession_sort = array($array[7]);
			break;
			case 129:
				return $this->profession_sort = array($array[8]);
			break;
			default:
				return $this->profession_sort = false;
		}
	}

	public function getMiscQuestSortId()
	{
		// Epic, Seasonal, Treasure Map, Special, Legendary, Darkmoon Faire, Lunar Festival, Reputation, Invasion, Midsummer
		return array(-1, -22, -221, -284, -344, -364, -366, -367, -368, -369);
	}

	public function getMiscQuestSortIdFromMiscId($misc_id)
	{
		return $this->misc_sort = array('-'.$misc_id);
	}

	public function getSortInfoBySort($sort, $classorskill = null)
	{
		$quest = new Quest;
		$classes = new Classes;

		$where_id = implode(' OR `id`='.'-', $sort);

		$db = new Database('dbh1');
		$db->select('dbh1', 'questsort', 'id, sort_name', '`id`='.'-'.$where_id);
		$result = $db->getResult();

		if($classorskill == 'class')
		{
			$db = new Database('dbh1');
			$db->select('dbh1', 'chrclasses', 'id, name');
			$chrsort = $db->getResult();
		}elseif($classorskill == 'skill')
		{
			$db = new Database('dbh1');
			$db->select('dbh1', 'skillline', 'id, name');
			$chrsort = $db->getResult();
		}

		for($i = 0; $i < count($sort); $i++)
		{
			for($n = 0; $n < count($result); $n++)
			{
				if('-'.$result[$n]['id'] == $sort[$i])
				{
					$this->sort[$i]['sort_id'] = '-'.$result[$n]['id'];
					$this->sort[$i]['sort_name'] = $result[$n]['sort_name'];
				}
			}

			for($n = 0; $n < count(@$chrsort); $n++)
			{
				if($chrsort[$n]['name'] == $this->sort[$i]['sort_name'])
					$this->sort[$i]['id'] = $chrsort[$n]['id'];
			}
		}

		// get quests
		$where = '`zoneorsort`='.implode(' OR `zoneorsort`=', $sort);

		if($classorskill == 'class')
		{
			for($i = 0; $i < count($this->getClassId(false)); $i++)
			{
				$array = $this->getClassId(false);
				$reqclass[] = $array[$i]['RequiredClasses'];
			}
			$where .= ' OR `RequiredClasses`='.implode(' OR `RequiredClasses`=', $reqclass);
		}

		$db = new Database('dbh');
		$db->select('dbh', 'quest_template', 'entry, zoneorsort, RequiredClasses', $where);
		$quests = $db->getResult();

		if(count($quests) > 0)
		{
			for($i = 0; $i < count($quests); $i++)
			{
				$quests_array[] = $quests[$i]['entry'];
				$quests[$i]['status'] = -1;
			}

			$where = implode(' OR `entry`=', $quests_array);
			$db = new Database('dbh1');
			$db->select('dbh1', 'testing_entry_status', '*', '(`entry`='.$where.') AND `type`='.QUEST);
			$quest_status = $db->getResult();

			for($n = 0; $n < count($quests); $n++)
			{
				for($i = 0; $i < count($quest_status); $i++)
				{
					if($quest_status[$i]['entry'] == $quests[$n]['entry'])
					{
						$quests[$n]['status'] = $quest_status[$i]['status'];
					}
				}
			}
		}

		$total_quests = 0;
		$total_tested = 0;
		for($i = 0; $i < count($sort); $i++)
		{
			$quests_array = array();

			// loop through all quests, push correct ones into correct quests_array
			for($n = 0; $n < count($quests); $n++)
			{
				if($sort[$i] == $quests[$n]['zoneorsort'] || (isset($this->sort[$i]['id']) && ($classes->getRequiredClassesFromClassId($this->sort[$i]['id']) == $quests[$n]['RequiredClasses'])))
				{
					$quests_array[] = array('entry' => $quests[$n]['entry'], 'status' => $quests[$n]['status']);
				}
			}
			$this->sort[$i]['quests'] = $quests_array;
			$this->sort[$i]['numQuests'] = count($this->sort[$i]['quests']);

			$progress = 0;
			for($n = 0; $n < count($this->sort[$i]['quests']); $n++)
			{
				if($this->sort[$i]['quests'][$n]['status'] != UNTESTED && $this->sort[$i]['quests'][$n]['status'] != NEEDS_MORE_TESTING)
					$progress++;
			}

			$this->sort[$i]['questProgTested'] = $progress;

			$total_quests += $this->sort[$i]['numQuests'];
			$total_tested += $progress;

			if($this->sort[$i]['numQuests'] > 0)
			{
				if($progress > 0)
					$this->sort[$i]['questProgPct'] = round((($progress) / ($this->sort[$i]['numQuests'])) * (100), 0, PHP_ROUND_HALF_DOWN);
				else
					$this->sort[$i]['questProgPct'] = 0;
			}else
			{
				$this->sort[$i]['questProgPct'] = 100;
			}
		}

		$sort_total['numQuests'] = $total_quests;
		if($sort_total['numQuests'] > 0)
			$sort_total['questProgPct'] = round((($total_tested) / ($total_quests)) * (100), 0, PHP_ROUND_HALF_DOWN);
		else
			$sort_total['questProgPct'] = 100;

		$this->sort_list = $this->sort;
		$this->sort_total = $sort_total;
	}
}
?>