<?php
class Database
{
	public $dbh;
    public $dbh1;
    public $dbh2;
	private static $instance;

	public $numResults;
    private $result = array();          // Results that are returned from the query

	public function __construct($con)
	{
		try
		{
            switch($con)
            {
                case 'dbh':
        			$this->dbh = new PDO(DB_TYPE.':host='.DB_HOST.';dbname='.DB_NAME.';charset=utf8', DB_USER, DB_PASS);
        			$this->dbh->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        			$this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                break;
                case 'dbh1':
                    $this->dbh1 = new PDO(DB_TYPE1.':host='.DB_HOST1.';dbname='.DB_NAME1.';charset=utf8', DB_USER1, DB_PASS1);
                    $this->dbh1->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
                    $this->dbh1->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                break;
                case 'dbh2':
                    $this->dbh2 = new PDO(DB_TYPE2.':host='.DB_HOST2.';dbname='.DB_NAME2.';charset=utf8', DB_USER2, DB_PASS2);
                    $this->dbh2->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
                    $this->dbh2->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                break;
                case 'dbh3':
                    $this->dbh3 = new PDO(DB_TYPE3.':host='.DB_HOST3.';dbname='.DB_NAME3.';charset=utf8', DB_USER3, DB_PASS3);
                    $this->dbh3->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
                    $this->dbh3->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                break;
                case 'dbh4':
                    $this->dbh4 = new PDO(DB_TYPE4.':host='.DB_HOST4.';dbname='.DB_NAME4.';charset=utf8', DB_USER4, DB_PASS4);
                    $this->dbh4->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
                    $this->dbh4->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                break;
            }
		}
		catch (PDOException $e)
		{
			die("Database Error: ". $e->getMessage() . "<br />");
			
		}
	}

    public static function getInstance($con)
    {

            $object = __CLASS__;
            self::$instance = new $object($con);
        return self::$instance;
    }

    private function tableExists($table, $con)
    {
        switch($con)
        {
            case 'dbh':
            $db_name = DB_NAME;
            break;
            case 'dbh1':
            $db_name = DB_NAME1;
            break;
            case 'dbh2':
            $db_name = DB_NAME2;
            break;
            case 'dbh3':
            $db_name = DB_NAME3;
            break;
            case 'dbh4':
            $db_name = DB_NAME4;
            break;
        }

        return true;

		$database = Database::getInstance($con);

        if(is_array($table))
        {
            for($i = 0; $i < count($table); $i++)
            {
        		$tablesInDb = $database->$con->prepare('SHOW TABLES FROM '.$db_name.' LIKE "'.$table[$i].'"');
        		$tablesInDb->execute();
        		$rowCount = $tablesInDb->rowCount();

        		if($tablesInDb)
        		{
        			if($rowCount <> 1)
        			{
        				die('Error: Table does not exist'.$table[$i]);
        			}
        		}
            }
        }else
        {
            $tablesInDb = $database->$con->prepare('SHOW TABLES FROM '.$db_name.' LIKE "'.$table.'"');
            $tablesInDb->execute();
            $rowCount = $tablesInDb->rowCount();

            if($tablesInDb)
            {
                if($rowCount <> 1)
                {
                    die('Error: Table does not exist'.$table);
                }
            }
        }

        return true;
    }

    public function select($con, $table, $rows = '*', $where = null, $order = null, $join = null, $limit = null, $group = null)
    {
		if($this->tableExists($table, $con))
		{
	    	if(is_array($table))
	    		$table = implode(', ', $table);

	    	$query = 'SELECT '.$rows.' FROM '.$table.'';

	    	if($join != null)
				$query .= ' INNER JOIN '.$join;
	    	if($where != null)
	    		$query .= ' WHERE '.$where;
			if($order != null)
				$query .= ' ORDER BY '.$order;
			if($limit != null)
				$query .= ' LIMIT '.$limit;
            if($group != null)
                $query .= ' GROUP BY '.$group;

			$database = Database::getInstance($con);
			$query = $database->$con->prepare($query);
			$query->execute();

			if($query)
			{
				$this->numResults = $query->rowCount();
					$result = $query->fetchAll(PDO::FETCH_ASSOC);
					$key = array_keys($result);
					for($x = 0; $x < count($key); $x++)
					{
						// Sanitizes keys so only alphavalues are allowed
						if(is_int($key[$x]))
						{
							if($query->rowCount() > 0)
								$this->result[$key[$x]] = $result[$key[$x]];
							elseif($query->rowCount() < 1)
								$this->result = null;
						}
					}
				return true;
			}else
			{
				return false;
			}
		}else
		{
			return false;
		}
    }

    public function insert($con, $table, $values, $cols = null, $multi_rows = false)
    {
        if($this->tableExists($table, $con))
        {
            $insert = 'INSERT INTO '.$table;

            if($cols != null)
            {
                $cols = implode(', ', $cols);
                $insert.= '('.$cols.')';
            }

            if($multi_rows == false)
            {
                for($i = 0; $i < count($values); $i++)
                {
                    //if(is_string($values[$i]))
                        $count[$i] = '?';
                }

                $count = implode(', ', $count);
                $insert .= ' VALUES ('.$count.')';
            }else
            {
                $insert .= ' VALUES '.$values;
            }

            $database = Database::getInstance($con);
            $insert = $database->$con->prepare($insert);
            
            if($multi_rows == false)
            {
                for($i = 0; $i < count($values); $i++)
                {
                    $insert->bindParam($i+1, $values[$i]);
                }
            }
            
            $insert->execute();
            
            if($insert)
            {
                return true;
            }else
            {
                return false;
            }
        }
    }

    public function update($con, $table, $rows, $where)
    {
        if($this->tableExists($table, $con))
        {
            // Parse the where values
            // even values (including 0) contain the where rows
            // odd values contain the clauses for the row
            for($i = 0; $i < count($where); $i++)
            {
                if($i%2 != 0)
                {
                    if(is_string($where[$i]))
                    {
                        if(($i+1) != null)
                            $where[$i] = '"'.$where[$i].'" AND ';
                        else
                            $where[$i] = '"'.$where[$i].'"';
                    }
                }
            }
            $where = implode('',$where);


            $update = 'UPDATE '.$table.' SET ';
            $keys = array_keys($rows);
            for($i = 0; $i < count($rows); $i++)
            {
                if(is_string($rows[$keys[$i]]))
                {
                    $update .= $keys[$i].'="'.$rows[$keys[$i]].'"';
                }
                else
                {
                    $update .= $keys[$i].'='.$rows[$keys[$i]];
                }

                // Parse to add commas
                if($i != count($rows)-1)
                {
                    $update .= ',';
                }
            }
            $update .= ' WHERE '.$where;

			$database = Database::getInstance($con);
			$query = $database->$con->prepare($update);
			$query->execute();

            if($query)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    public function delete($con, $table, $where = null)  
    {  
        if($this->tableExists($table, $con))  
        {  
            if($where == null)  
            {
                //$delete = 'DELETE '.$table;
                return false;
            }
            else
            {
                $delete = 'DELETE FROM '.$table.' WHERE '.$where;
            }
            
            $database = Database::getInstance($con);
            $query = $database->$con->prepare($delete);
            $query->execute();
            
            if($query)  
            {  
                return true;   
            }  
            else  
            {  
               return false;   
            }
        }
        else  
        {  
            return false;   
        }
    }

    public function truncate($con, $table)
    {  
        if($this->tableExists($table, $con))  
        {
            $truncate = 'TRUNCATE TABLE '.$table;
            
            $database = Database::getInstance($con);
            $query = $database->$con->prepare($truncate);
            $query->execute();
            
            if($query) 
            {  
                return true;   
            }  
            else  
            {  
               return false;
            }
        }
        else  
        {  
            return false;   
        }
    }

	public function getResult()
    {
        return $this->result;
    }
}

?>