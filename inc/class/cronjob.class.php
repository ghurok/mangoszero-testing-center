<?php
class Cronjob
{
	public function updateEntryStatuses($type)
	{
		$db = new Database('dbh1');
		$db->select('dbh1','testing_reports', 'id, status, type, entry', '`type`='.$type, '`entry` ASC');
		$reports = $db->getResult();
		$numResultsReports = $db->numResults;

		$db = new Database('dbh1');
		$db->select('dbh1','testing_reports', 'id, status, type, entry', '`type`='.$type, null, null, null, '`entry`');
		$entries = $db->getResult();
		$numResultsEntries = $db->numResults;

		/*$time = microtime();
		$time = explode(' ', $time);
		$time = $time[1] + $time[0];
		$start = $time;*/

		$entry_array = array();
		for($i = 0; $i < count($entries); $i++)
		{
			$status_array = array();

			$entry_array[$i]['entry'] = $entries[$i]['entry'];
			$entry_array[$i]['type'] = $entries[$i]['type'];
			$status_array[] = $entries[$i]['status'];

			for($n = 0; $n < count($reports); $n++)
			{
				if($entries[$i]['type'] == $reports[$n]['type'] && $entries[$i]['entry'] == $reports[$n]['entry'] && $entries[$i]['id'] != $reports[$n]['id'])
				{
					$status_array[] = $reports[$n]['status'];
				}
			}

			$entry_array[$i]['status'] = $status_array;
		}

		/*$time = microtime();
		$time = explode(' ', $time);
		$time = $time[1] + $time[0];
		$finish = $time;
		$total_time = round(($finish - $start), 4);
		echo 'Page generated in '.$total_time.' seconds.';*/

		$count_loop = $entry_array;
		$loop_switch = $entry_array;
		for($i = 0; $i < count($entry_array); $i++)
		{
			$status = 0;
			switch($entry_array[$i]['type'])
			{
				case ITEM:
					include '/home/projects/public_html/database/inc/item_status_calc.php';
				break;
				case NPC:
					include '/home/projects/public_html/database/inc/npc_status_calc.php';
				break;
				case _OBJECT:
					include '/home/projects/public_html/database/inc/object_status_calc.php';
				break;
				case SPELL:
					include '/home/projects/public_html/database/inc/spell_status_calc.php';
				break;
				case QUEST:
					include '/home/projects/public_html/database/inc/quest_status_calc.php';
				break;
			}
			$entry_array[$i]['status'] = $status;
		}

		$values_array = array();
		for($i = 0; $i < count($entry_array); $i++)
		{
			if(is_array($entry_array[$i]['status']))
				$entry_array[$i]['status'] = -1;

			$values_array[$i] = '('.implode(', ', $entry_array[$i]).')';
		}

		$values_array = implode(', ', $values_array).';';

		$db = new Database('dbh1');
		$db->delete('dbh1','testing_entry_status', '`type`='.$type);
		$db->insert('dbh1', 'testing_entry_status', $values_array, array('entry', 'type', 'status'), $multi_rows = true);
	}

	public function updateStatistics()
	{
		$user = new User;
		$reports = new Report;
		
		$user->getCharacters();
		$user->getUser();
		$reports->getReports(1, false, false, false, null);

		$db = new Database('dbh1');
		$db->update('dbh1', 'statistics', array('value' => $user->characters['numChars']), array('name="numCharacters"'));
		$db->update('dbh1', 'statistics', array('value' => $reports->numResults), array('name="numReports"'));
		$db->update('dbh1', 'statistics', array('value' => $user->numResults), array('name="numAccounts"'));
	}
}
?>