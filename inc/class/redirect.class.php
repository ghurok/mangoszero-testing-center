<?php
class Redirect
{
	public $success;
	public $success_id;
	
	public $error;
	public $errorid;

	public function doRedirect($url, $wait = 0, $success = null, $error = null)
	{
		if($success != null)
		{
			$succ_error = '&s='.$success;
		}

		if($error != null)
		{
			$succ_error = '&e='.$error;
		}

		header('Refresh: '.$wait.';url='.$url.''.@$succ_error.'');
	}

	public function success($id)
	{
		if(is_numeric($id))
		{
			switch($id)
			{
				case 1:
					$this->success = 'Your report has been submitted. Thank you! <br> It may take up to 2 minutes before the status of the entry is updated.';
				break;
				case 2:
					$this->success = 'The report has been deleted. You will be redirected soon.';
				break;
				case 3:
					$this->success = 'The report has been marked as fixed. You will be redirected soon.';
				break;
				case 4:
					$this->success = 'You have been logged in.';
				break;
				case 5:
					$this->success = 'You have been logged out.';
				break;
				case 6:
					$this->success = 'A game account has been created! Thank you for being apart of the project.';
				break;
				default:
					die('The value of "s" didn\'t match an existing success ID. You\'ll be redirected soon.');
			}
		}
	}

	public function error($id)
	{
		if(is_numeric($id))
		{
			switch($id)
			{
				case 1:
					$this->error = 'You forgot to read the instructions! You will be redirected back soon. Don\'t worry, your comment will still be filled in.';
				break;
				case 2:
					$this->error = 'Something went wrong with the entry parameter!';
				break;
				case 3:
					$this->error = 'You must specify a DB revision! You will be redirected back soon. Don\'t worry, your comment will still be filled in.';
				break;
				case 4:
					$this->error = 'You must fill in all fields.';
				break;
				case 5:
					$this->error = 'Your entered passwords did not match each other!';
				break;
				case 6:
					$this->error = 'You have used disallowed characters in your username.';
				break;
				case 7:
					$this->error = 'Username is already taken.';
				break;
				case 8:
					$this->error = 'Username and/or password was incorrect.';
				break;
				case 9:
					$this->error = 'Username and/or password was longer than 16 characters!';
				break;
				default:
					die('The value of "e" didn\'t match an existing error ID. You\'ll be redirected soon.');
			}
		}
	}

	public function currentUrl()
	{
	    $protocol = strpos(strtolower($_SERVER['SERVER_PROTOCOL']),'https') === FALSE ? 'http' : 'https';
	    $host     = $_SERVER['HTTP_HOST'];
	    $script   = $_SERVER['SCRIPT_NAME'];
	    $params   = $_SERVER['QUERY_STRING'];

	    return $protocol . '://' . $host . $script . '?' . $params;
	    //return $_SERVER['REQUEST_URI'];
	}
}

?>