<?php
class WoW
{
	public function getRace($id)
	{
		switch($id)
		{
			case 1:
				return 'Human';
			break;
			case 2:
				return 'Orc';
			break;
			case 3:
				return 'Dwarf';
			break;
			case 4:
				return 'Night Elf';
			break;
			case 5:
				return 'Undead';
			break;
			case 6:
				return 'Tauren';
			break;
			case 7:
				return 'Gnome';
			break;
			case 8:
				return 'Troll';
			break;
		}
	}

	public function getClass($id)
	{
		switch($id)
		{
			case 1:
				return 'Warrior';
			break;
			case 2:
				return 'Paladin';
			break;
			case 3:
				return 'Hunter';
			break;
			case 4:
				return 'Rogue';
			break;
			case 5:
				return 'Priest';
			break;
			case 7:
				return 'Shaman';
			break;
			case 8:
				return 'Mage';
			break;
			case 9:
				return 'Warlock';
			break;
			case 11:
				return 'Druid';
			break;
		}
	}

	public function getGender($id)
	{
		switch($id)
		{
			case 0:
				return 'Male';
			break;
			case 1:
				return 'Female';
			break;
		}
	}
}
?>