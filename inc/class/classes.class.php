<?php
class Classes
{
	public $classes_list;
	public $classes_total;

	public $classes;

	public function getClass($class, $all = false)
	{
		$db = new Database('dbh1');
		$db->select('dbh1', 'areatable', '`id`, `COL 11`', '`COL 3`='.$zone);
		$result = $db->getResult();
		$numResults = $db->numResults;
		for($i = 0; $i < count($result); $i++)
		{
		}
	}

	public function getClassQuestSortId()
	{
		return array(-61, -82, -161, -261, -263, -81, -141, -162, -262);
	}

	public function getRequiredClassesFromClassId($class_id = false)
	{
		$classes[1]['RequiredClasses'] = 1;
		$classes[2]['RequiredClasses'] = 2;
		$classes[3]['RequiredClasses'] = 4;
		$classes[4]['RequiredClasses'] = 8;
		$classes[5]['RequiredClasses'] = 16;
		$classes[7]['RequiredClasses'] = 64;
		$classes[8]['RequiredClasses'] = 128;
		$classes[9]['RequiredClasses'] = 256;
		$classes[11]['RequiredClasses'] = 1024;

		return $classes[$class_id]['RequiredClasses'];
	}

	public function getClassInfoByClass($classes)
	{
		$quest = new Quest;

		$where_id = implode(' OR `id`='.'-', $classes);

		$db = new Database('dbh1');
		$db->select('dbh1', 'questsort', 'id, sort_name', '`id`='.'-'.$where_id);
		$result = $db->getResult();

		$db = new Database('dbh1');
		$db->select('dbh1', 'chrclasses', 'id, name');
		$chrclasses = $db->getResult();

		for($i = 0; $i < count($classes); $i++)
		{
			for($n = 0; $n < count($result); $n++)
			{
				if('-'.$result[$n]['id'] == $classes[$i])
				{
					$this->classes[$i]['sort_id'] = '-'.$result[$n]['id'];
					$this->classes[$i]['sort_name'] = $result[$n]['sort_name'];
				}
			}

			for($n = 0; $n < count($chrclasses); $n++)
			{
				if($chrclasses[$n]['name'] == $this->classes[$i]['sort_name'])
					$this->classes[$i]['id'] = $chrclasses[$n]['id'];
			}
		}

		// get quests
		$where = implode(' OR `zoneorsort`=', $classes);
		$db = new Database('dbh');
		$db->select('dbh', 'quest_template', 'entry, zoneorsort', '`zoneorsort`='.$where);
		$quests = $db->getResult();

		if(count($quests) > 0)
		{
			for($i = 0; $i < count($quests); $i++)
			{
				$quests_array[] = $quests[$i]['entry'];
				$quests[$i]['status'] = -1;
			}

			$where = implode(' OR `entry`=', $quests_array);
			$db = new Database('dbh1');
			$db->select('dbh1', 'testing_entry_status', '*', '(`entry`='.$where.') AND `type`='.QUEST);
			$quest_status = $db->getResult();

			for($n = 0; $n < count($quests); $n++)
			{
				for($i = 0; $i < count($quest_status); $i++)
				{
					if($quest_status[$i]['entry'] == $quests[$n]['entry'])
					{
						$quests[$n]['status'] = $quest_status[$i]['status'];
					}
				}
			}
		}

		$total_quests = 0;
		$total_tested = 0;
		for($i = 0; $i < count($classes); $i++)
		{
			$quests_array = array();
			for($n = 0; $n < count($quests); $n++)
			{
				if($classes[$i] == $quests[$n]['zoneorsort'])
				{
					$quests_array[] = array('entry' => $quests[$n]['entry'], 'status' => $quests[$n]['status']);
				}
			}
			$this->classes[$i]['quests'] = $quests_array;
			$this->classes[$i]['numQuests'] = count($this->classes[$i]['quests']);

			$progress = 0;
			for($n = 0; $n < count($this->classes[$i]['quests']); $n++)
			{
				if($this->classes[$i]['quests'][$n]['status'] != UNTESTED && $this->classes[$i]['quests'][$n]['status'] != NEEDS_MORE_TESTING)
					$progress++;
			}

			$this->classes[$i]['questProgTested'] = $progress;

			$total_quests += $this->classes[$i]['numQuests'];
			$total_tested += $progress;

			if($this->classes[$i]['numQuests'] > 0)
			{
				if($progress > 0)
					$this->classes[$i]['questProgPct'] = round((($progress) / ($this->classes[$i]['numQuests'])) * (100), 0, PHP_ROUND_HALF_DOWN);
				else
					$this->classes[$i]['questProgPct'] = 0;
			}else
			{
				$this->classes[$i]['questProgPct'] = 100;
			}
		}

		$classes_total['numQuests'] = $total_quests;
		if($classes_total['numQuests'] > 0)
			$classes_total['questProgPct'] = round((($total_tested) / ($total_quests)) * (100), 0, PHP_ROUND_HALF_DOWN);
		else
			$classes_total['questProgPct'] = 100;

		$this->classes_list = $this->classes;
		$this->classes_total = $classes_total;
	}
}
?>