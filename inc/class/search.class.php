<?php
class Search
{
	public function search()
	{
		$type = $_POST['search_type'];
		$search_string = $_POST['search_string'];

		switch($type)
		{
			case ITEM:
			header('Location: items.php?q='.$search_string);
			break;
			case NPC:
			header('Location: npcs.php?q='.$search_string);
			break;
			case _OBJECT:
			header('Location: objects.php?q='.$search_string);
			break;
			case SPELL:
			header('Location: spells.php?q='.$search_string);
			break;
			case QUEST:
			header('Location: quests.php?q='.$search_string);
			break;
		}
	}
}
?>