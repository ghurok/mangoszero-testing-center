<?php

class Npc
{
	public $npc;
	public $npc_guid;
	public $status;
	public $numResults = 0;

	public function getNpcs($list = false, $where = null, $join = null, $limit = null)
	{
		if($list == true)
			$rows = '`entry`, `name`';
		else
			$rows = '*';

		$db = new Database('dbh');
		$db->select('dbh','creature_template', $rows, ''.$where.'', null, ''.$join.'', $limit);
		$result = $db->getResult();
		$numResults = $db->numResults;
		if($numResults >= 1)
		{
			$this->numResults = $numResults;
			return $this->npc = $result;
		}else
		{
			return false;
		}
	}

	public function getStatus($entry = false)
	{
		$db = new Database('dbh1');
		$db->select('dbh1','testing_entry_status', 'entry, status', '(`entry`='.$entry.') AND `type`='.NPC);
		$result = $db->getResult();
		$numResults = $db->numResults;

		switch(@$result[0]['status'])
		{
			case NPC_NOT_WORKING_AS_INTENDED:
				if(@$text)
					$status = NOT_WORKING_AS_INTENDED;
				else
					$status = '<span class="tag tag2">'.NOT_WORKING_AS_INTENDED.'</span>';
			break;
			case NPC_OBSOLETE:
				if(@$text)
					$status = OBSOLETE;
				else
					$status = '<span class="tag tag2">'.OBSOLETE.'</span>';
			break;
			case NPC_WORKING_PERFECTLY:
				if(@$text)
					$status = WORKING_PERFECTLY;
				else
					$status = '<span class="tag tag8">'.WORKING_PERFECTLY.'</span>';
			break;
			case NPC_NO_BUGS_FOUND:
				if(@$text)
					$status = NO_BUGS_FOUND;
				else
					$status = '<span class="tag tag4">'.NO_BUGS_FOUND.'</span>';
			break;
			case NEEDS_MORE_TESTING:
				if(@$text)
					$status = NEEDS_MORE_TESTING_TEXT;
				else
					$status = '<span class="tag tag_nmt">'.NEEDS_MORE_TESTING_TEXT.'</span>';
			break;
			default:
				if(@$text)
					$status = UNTESTED_TEXT;
				else
					$status = '<span class="tag tag0">'.UNTESTED_TEXT.'</span>';
		}

		return $this->status = $status;

		/*$report = new Report;
		$report->getReports(null, $entry, NPC);

		if($report->numResults > 0)
		{
			$NPC_NOT_WORKING_AS_INTENDED = 0;
			$NPC_NO_BUGS_FOUND = 0;
			$NPC_WORKING_PERFECTLY = 0;
			$NPC_OBSOLETE = 0;

			foreach($report->report as $report):
				switch($report['status'])
				{
					case NPC_NOT_WORKING_AS_INTENDED:
						$NPC_NOT_WORKING_AS_INTENDED++;
					break;
					case NPC_OBSOLETE:
						$NPC_OBSOLETE++;
					break;
					case NPC_NO_BUGS_FOUND:
						$NPC_NO_BUGS_FOUND++;
					break;
					case NPC_WORKING_PERFECTLY:
						$NPC_WORKING_PERFECTLY++;
					break;
				}
			endforeach;

			if(@$NPC_NOT_WORKING_AS_INTENDED >= NPC_NOT_WORKING_AS_INTENDED)
				return $this->status = '<span class="tag tag2">'.NOT_WORKING_AS_INTENDED.'</span>';
			elseif(@$NPC_WORKING_PERFECTLY >= NPC_WORKING_PERFECTLY)
				return $this->status = '<span class="tag tag8">'.WORKING_PERFECTLY.'</span>';
			elseif(@$NPC_NO_BUGS_FOUND >= NPC_NO_BUGS_FOUND)
				return $this->status = '<span class="tag tag4">'.NO_BUGS_FOUND.'</span>';
			elseif(@$NPC_OBSOLETE >= NPC_OBSOLETE)
				return $this->status = '<span class="tag tag2">'.OBSOLETE.'</span>';
			elseif(@$NPC_NOT_WORKING_AS_INTENDED > 0 || @$NPC_NO_BUGS_FOUND > 0  || @$NPC_WORKING_PERFECTLY > 0 || @$NPC_OBSOLETE > 0)
				return $this->status = '<span class="tag tag_nmt">'.NEEDS_MORE_TESTING.'</span>';
			else
				return $this->status = '<span class="tag tag0">'.UNTESTED.'</span>';
		}else
		{
			return $this->status = '<span class="tag tag0">'.UNTESTED.'</span>';
		}*/
	}

	public function getGuids($entry, $guid = null)
	{
		if($guid != null)
			$where = 'guid='.$guid;
		else
			$where = 'id='.$entry;

		$db = new Database('dbh');
		$db->select('dbh','creature', '*', ''.$where.'');
		$result = $db->getResult();
		$numResults = $db->numResults;
		if($numResults > 0)
		{
			for($i = 0; $i < count($result); $i++)
			{
				switch($result[$i]['map'])
				{
					case 0:
						$result[$i]['map'] = $result[$i]['map'].' (Kalimdor)';
					break;
					case 1:
						$result[$i]['map'] = $result[$i]['map'].' (Eastern Kingdoms)';
					break;
				}
			}

			$this->numResults = $numResults;
			return $this->npc_guid = $result;
		}
	}
}

?>