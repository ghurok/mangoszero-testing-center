<?php
class Breadcrumb
{
	public function getBreadcrumbs()
	{
		switch($file = basename($_SERVER['PHP_SELF']))
		{
			case ($file == 'quest.php' || $file == 'quests.php'):
				$breadcrumb = '<a href="quests.php">Quests</a>';
			break;
			case ($file == 'item.php' || $file == 'items.php'):
				$breadcrumb = '<a href="items.php">Items</a>';
			break;
			case ($file == 'spell.php' || $file == 'spells.php'):
				$breadcrumb = '<a href="spells.php">Spells</a>';
			break;
			case ($file == 'object.php' || $file == 'objects.php'):
				$breadcrumb = '<a href="objects.php">Objects</a>';
			break;
			case ($file == 'npc.php' || $file == 'npcs.php'):
				$breadcrumb = '<a href="npcs.php">NPCs</a>';
			break;

			case 'profile.php':
				$breadcrumb = '<a href="profiles.php">Profiles</a>';
			break;
			case ($file == 'zone.php' || $file == 'zones.php'):
				$breadcrumb = '<a href="zones.php">Zones</a>';
			break;
			case 'phases.php':
				$breadcrumb = '<a href="phases.php">Testing Phases</a>';
			break;
			case 'professions.php':
				$breadcrumb = '<a href="professions.php">Professions</a>';
			break;
			case ($file == 'class.php' || $file == 'classes.php'):
				$breadcrumb = '<a href="classes.php">Classes</a>';
			break;
			case 'skill.php':
				$breadcrumb = '<a href="skills.php">Skills</a>';
			break;
			case 'instances.php':
				$breadcrumb = '<a href="instances.php">Instances</a>';
			break;
			case 'misc.php':
				$breadcrumb = '<a href="miscs.php">Misc</a>';
			break;
			case 'login.php':
				$breadcrumb = '<a href="login.php">Log in</a>';
			break;
			case 'signup.php':
				$breadcrumb = '<a href="signup.php">Sign up</a>';
			break;
			case 'redirect.php':
				$breadcrumb = 'Redirect';
			break;
			
			default:
				$breadcrumb = '';
		}

		if($breadcrumb != '')
			return '<span class="breadcrumb-arrow">Testing Center</span>'.$breadcrumb;
		else
			return 'Testing Center';
	}
}
?>