<?php
class Paginator{
    var $items_per_page;
    var $items_total;
    var $current_page;
    var $num_pages;
    var $mid_range;
    var $low;
    var $high;
    var $limit;
    var $return;
    var $default_ipp = 25;

    var $show_min_out_of;
    var $show_max_out_of;

    var $query;
    var $page_para;
   
    function Paginator()
    {
        if(!empty($_GET['q']))
            $this->query = '&q='.$_GET['q'];

        if(!empty($_GET['page']))
            $this->page_para = 'page='.$_GET['page'];

        $this->current_page = 1;
        $this->mid_range = 7;

        // set how many items are shown per page
        if(!empty($_GET['ipp']))
        {
            if($_GET['ipp'] <= 50) // limit max items per page to 100
            {
                $this->items_per_page = $_GET['ipp'];

            }else
            {
                $this->items_per_page = 50; // max items per page
            }
        }else
        {
            $this->items_per_page = $this->default_ipp; // if ipp is not set in URL, use default ipp
        }
    }

    function paginate()
    {
        // run if items per page value is not numeric or value is negative
        if(!is_numeric($this->items_per_page) OR $this->items_per_page <= 0) 
            $this->items_per_page = $this->default_ipp; // items per page is default ipp
        
        // calculate number of pages
        $this->num_pages = ceil($this->items_total/$this->items_per_page);

        // set the current page to $_GET['page_num']
        if(isset($_GET['page_num']))
            $this->current_page = $_GET['page_num']; // must be numeric > 0
        
        // set current page to 1 if current page is (0 or negative) or is not numeric
        if($this->current_page < 1 Or !is_numeric($this->current_page)) $this->current_page = 1;
        
        // if current page exceeds total number of pages, set current page to total number of pages
        if($this->current_page > $this->num_pages) $this->current_page = $this->num_pages;
        $prev_page = $this->current_page-1; // set previous page number
        $next_page = $this->current_page+1; // set next page number
       
        if($this->num_pages > 3)
        {
            // concatenate previous button
            $this->return = ($this->current_page != 1 And $this->items_total >= 10) ? "<a class=\"paginate classname previous\" href=\"?".$this->page_para.@$this->query."&page_num=$prev_page&ipp=$this->items_per_page\">Previous</a> ":"<span class=\"inactive classname previous\" href=\"#\">Previous</span> ";
            
            // number of pages show to the left before ...
            $this->start_range = $this->current_page - floor($this->mid_range)/1.5;

            // number of pages show to the right before ...
            $this->end_range = $this->current_page + floor($this->mid_range)/1.5;
            
            // run when there are not enough pages to the left
            if($this->start_range <= 0)
            {
                $this->end_range += abs($this->start_range)+1;
                $this->start_range = 1;
            }
            
            // run when there are not enough pages to the right
            if($this->end_range > $this->num_pages)
            {
                $this->start_range -= $this->end_range-$this->num_pages;
                $this->end_range = $this->num_pages;
            }
            
            // create an array with items ranging from start_range to end_range
            $this->range = range($this->start_range,$this->end_range);

            // loop through all pages
            for($i=1;$i<=$this->num_pages;$i++)
            {
                // concatenate ... to $this->return
                //if($this->range[0] > 2 And $i == $this->range[0]) $this->return .= " ... ";
                
                // loop through all pages. if first, last, or in range, display
                // ORIGINAL IF: if($i==1 Or $i==$this->num_pages Or in_array($i,$this->range))
                if(in_array($i,$this->range))
                {
                    $this->return .= ($i == $this->current_page) ? "<div class=\"numbers\"><a title=\"Go to page $i of $this->num_pages\" class=\"current\" href=\"#\">$i</a></div>":"<div class=\"numbers\"><a class=\"paginate\" title=\"Go to page $i of $this->num_pages\" href=\"?".$this->page_para.@$this->query."&page_num=$i&ipp=$this->items_per_page\">$i</a></div>";
                }

                // concatenate ... to $this->return
                //if($this->range[$this->mid_range-1] < $this->num_pages-1 And $i == $this->range[$this->mid_range-1]) $this->return .= " ... ";
            }
            // concatenate next button
            $this->return .= (($this->current_page != $this->num_pages And $this->items_total >= 10)) ? "<a class=\"paginate classname next\" href=\"?".$this->page_para.@$this->query."&page_num=$next_page&ipp=$this->items_per_page\">Next</a>\n":"<span class=\"inactive classname next\" href=\"#\">Next</span>\n";
        }
        else
        {
            // lopp through all pages if total number of pages is less than 10
            for($i=1;$i<=$this->num_pages;$i++)
            {
                $this->return .= ($i == $this->current_page) ? "<div class=\"numbers\"><a class=\"current\" href=\"#\">$i</a></div>":"<div class=\"numbers\"><a class=\"paginate\" href=\"?".$this->page_para.@$this->query."&page_num=$i&ipp=$this->items_per_page\">$i</a></div>";
            }
        }
        $this->low = ($this->current_page-1) * $this->items_per_page;
        $this->high = ($this->current_page * $this->items_per_page)-1;
        $this->limit = " $this->low,$this->items_per_page";

        // set 'show_min_out_of'
       	$this->show_min_out_of = ($this->items_per_page * $this->current_page) - ($this->items_per_page - 1);

        if($this->items_per_page < $this->items_total)
        {
            $this->show_max_out_of = ($this->items_per_page * $this->current_page);
        }else
        {
            $this->show_max_out_of = $this->items_total; // run if items_total is less than items_per_page
        }

        if($this->show_max_out_of > $this->items_total)
            $this->show_max_out_of = $this->items_total;
    }

    function display_items_per_page()
    {
        $items = '';
        $ipp_array = array(5,10,25,50);
        foreach($ipp_array as $ipp_opt)    $items .= ($ipp_opt == $this->items_per_page) ? "<option selected value=\"$ipp_opt\">$ipp_opt</option>\n":"<option value=\"$ipp_opt\">$ipp_opt</option>\n";
        return "<span class=\"paginate\">Entries per page: </span><select class=\"paginate\" onchange=\"window.location='?".$this->page_para.@$this->query."&page_num=1&ipp='+this[this.selectedIndex].value;return false\">$items</select>\n";
    }

    function display_jump_menu()
    {
        for($i=1;$i<=$this->num_pages;$i++)
        {
            $this->option .= ($i==$this->current_page) ? "<option value=\"$i\" selected>$i</option>\n":"<option value=\"$i\">$i</option>\n";
        }
        return "<span id=\"page_jump\" class=\"paginate\">Page: </span><select class=\"paginate\" onchange=\"window.location='?".$this->page_para.@$this->query."&page_num='+this[this.selectedIndex].value+'&ipp=$this->items_per_page';return false\">$this->option</select>\n";
    }

    function display_pages()
    {
        return $this->return;
    }
}
?>
