<?php
class GraphData
{
	public $num_per_month;
	public $highest_month;
	public $tick_num_month;
	public $tick_unnum_month;

	public $num_per_col_wbf;
	public $highest_wbf;
	public $tick_num_wbf;
	public $tick_unnum_wbf;

	public function getReportPerMonth($year, $user_id = null)
	{
		if($user_id != null)
			$where = 'date LIKE '.'"%'.$year.'%" AND user_id='.$user_id;
		else
			$where = 'date LIKE '.'"%'.$year.'%"';

		$db = new Database('dbh1');
		$db->select('dbh1', 'testing_reports', 'date', $where);
		$result = $db->getResult();
		$numResults = $db->numResults;

		$jan = $feb = $mar = $apr = $may = $jun = $jul = $aug = $sep = $oct = $nov = $dec = 0;

		for($i = 0; $i < count($result); $i++)
		{
			switch(true)
			{
				case substr($result[$i]['date'], 0, -3) == $year.'-01':
					$jan = $jan + 1;
				break;
				case substr($result[$i]['date'], 0, -3) == $year.'-02':
					$feb = $feb + 1;
				break;
				case substr($result[$i]['date'], 0, -3) == $year.'-03':
					$mar = $mar + 1;
				break;
				case substr($result[$i]['date'], 0, -3) == $year.'-04':
					$apr = $apr + 1;
				break;
				case substr($result[$i]['date'], 0, -3) == $year.'-05':
					$may = $may + 1;
				break;
				case substr($result[$i]['date'], 0, -3) == $year.'-06':
					$jun = $jun + 1;
				break;
				case substr($result[$i]['date'], 0, -3) == $year.'-07':
					$jul = $jul + 1;
				break;
				case substr($result[$i]['date'], 0, -3) == $year.'-08':
					$aug = $aug + 1;
				break;
				case substr($result[$i]['date'], 0, -3) == $year.'-09':
					$sep = $sep + 1;
				break;
				case substr($result[$i]['date'], 0, -3) == $year.'-10':
					$oct = $oct + 1;
				break;
				case substr($result[$i]['date'], 0, -3) == $year.'-11':
					$nov = $nov + 1;
				break;
				case substr($result[$i]['date'], 0, -3) == $year.'-12':
					$dec = $dec + 1;
				break;
			}
		}

		$this->num_per_month = array($jan, $feb, $mar, $apr, $may, $jun, $jul, $aug, $sep, $oct, $nov, $dec);
		$this->highest_month = max($this->num_per_month);

		for($i = 0; $i < 11; $i++)
		{
			$pct = $i * 0.1;

			$this->tick_num_month[] = round(($this->highest_month * $pct), -1);
		}

		for($i = 1; $i < count($this->tick_num_month); $i++)
		{
			$this->tick_unnum_month[] = round((($this->tick_num_month[$i]) * 0.5), -1);
		}
	}

	public function getWorksBuggedFixed()
	{
		$db = new Database('dbh1');

		// works
		$db->select('dbh1', 'testing_reports', 'status', 'status IN ('
			.ITEM_NO_BUGS_FOUND.','.ITEM_WORKING_PERFECTLY.','.SPELL_NO_BUGS_FOUND.','.OBJECT_NO_BUGS_FOUND.','.OBJECT_WORKING_PERFECTLY.','.NPC_NO_BUGS_FOUND.','
			.NPC_WORKING_PERFECTLY.','.QUEST_COMPLETABLE_NO_BUGS_FOUND.','.QUEST_WORKING_PERFECTLY.')');
		$result = $db->getResult();
		$num_works = $db->numResults;

		// bugged
		$db->select('dbh1', 'testing_reports', 'status', 'status IN ('
			.ITEM_NOT_WORKING_AS_INTENDED.','.SPELL_NOT_WORKING_AS_INTENDED.','.OBJECT_NOT_WORKING_AS_INTENDED.','.NPC_NOT_WORKING_AS_INTENDED.','.QUEST_BUGS_FOUND.','.QUEST_NOT_COMPLETABLE.')');
		$result = $db->getResult();
		$num_bugged = $db->numResults;

		// fixed
		$db->select('dbh1', 'testing_reports', 'status', 'status IN ('.FIXED.')');
		$result = $db->getResult();
		$num_fixed = $db->numResults;

		$this->num_per_col_wbf = array($num_works, $num_bugged, $num_fixed);
		$this->highest_wbf = max($this->num_per_col_wbf);

		for($i = 0; $i < 11; $i++)
		{
			$pct = $i * 0.1;

			$this->tick_num_wbf[] = round(($this->highest_wbf * $pct), -1);
		}

		for($i = 1; $i < count($this->tick_num_wbf); $i++)
		{
			$this->tick_unnum_wbf[] = round((($this->tick_num_wbf[$i]) * 0.5), -1);
		}
	}
}
?>