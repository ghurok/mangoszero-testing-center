<?php
class Area
{
	public $zones_list;
	public $zones_total;

	public $zones_array;
	public $quest_zoneorsort;

	public function getArea($area, $zone_subzone = false)
	{
		$db = new Database('dbh1');
		$db->select('dbh1','areatable', $rows, ''.$where.'', null, ''.$join.'', $limit);
		$result = $db->getResult();
		$numResults = $db->numResults;
	}

	public function getSubZones($zones)
	{
		$where_id = implode(' OR `id`=', $zones);
		$where_col3 = implode(' OR `COL 3`=', $zones);

		$db = new Database('dbh1');
		$db->select('dbh1', 'areatable', 'id, `COL 3`, zoneorsort', '`id`='.$where_id.' OR `COL 3`='.$where_col3);
		$result = $db->getResult();
		$numResults = $db->numResults;

		for($i = 0; $i < count($zones); $i++)
		{
			$quest_zoneorsort[$i] = $zones[$i];

			for($n = 0; $n < count($result); $n++)
			{
				if($result[$n]['id'] == $zones[$i])
				{
					$this->zones_array[$i]['id'] = $result[$n]['id'];

					switch($result[$n]['id'])
					{
						case 3428: // Ahn'Qiraj (The Temple of Ahn'Qiraj)
							$this->zones_array[$i]['zone_name'] = $result[$n]['zoneorsort'].' (The Temple of Ahn\'Qiraj)';
						break;
						case 1477: // The Temple of Atal'Hakkar (Sunken Temple)
							$this->zones_array[$i]['zone_name'] = $result[$n]['zoneorsort'].' (Sunken Temple)';
						break;
						default:
							$this->zones_array[$i]['zone_name'] = $result[$n]['zoneorsort'];
					}

					$subzones_array = array();
					for($a = 0; $a < count($result); $a++)
					{
						if($result[$a]['COL 3'] == $result[$n]['id'])
						{
							$subzones_array[] = $result[$a]['id'];
							$quest_zoneorsort[] = $result[$a]['id'];
						}
					}
					$this->zones_array[$i]['subzones'] = $subzones_array;
					$this->zones_array[$i]['numSubZones'] = count($this->zones_array[$i]['subzones']);
				}
			}
		}
		$this->quest_zoneorsort = $quest_zoneorsort;
	}

	public function getDungeonsZoneId()
	{
		return array(2437, 1581, 718, 209, 717, 719, 721, 491, 796, 722, 1337, 1176, 2100, 1477, 1584, 1583, 2557, 2017, 2057);
	}

	public function getRaidsZoneId()
	{
		return array(2159, 1977, 2717, 3429, 2677, 3428, 3456);
	}

	public function getCitiesZoneId()
	{
		return array(1657, 1537, 1519, 1637, 1638, 1497);
	}

	public function getZoneInfoByZones($continent = null, $zones = null)
	{
		$quest = new Quest;

		if($continent !== null)
		{
			if($continent == 0) // eastern kingdoms
				$zones = array(33, 4, 41, 8, 10, 40, 44, 12, 46, 51, 3, 1, 38, 11, 45, 267, 47, 130, 36, 85, 139, 28);
			else // kalimdor
				$zones = array(440, 490, 1377, 400, 357, 15, 215, 405, 17, 406, 14, 331, 16, 15, 361, 148, 493, 618, 141);
		}

		$this->getSubZones($zones);

		// get quests
		$where = implode(' OR `zoneorsort`=', $this->quest_zoneorsort);
		$db = new Database('dbh');
		$db->select('dbh', 'quest_template', 'entry, zoneorsort', '`zoneorsort`='.$where);
		$quests = $db->getResult();

		if(count($quests) > 0)
		{
			for($i = 0; $i < count($quests); $i++)
			{
				$quests_array[] = $quests[$i]['entry'];
				$quests[$i]['status'] = -1;
			}

			$where = implode(' OR `entry`=', $quests_array);
			$db = new Database('dbh1');
			$db->select('dbh1', 'testing_entry_status', '*', '(`entry`='.$where.') AND `type`='.QUEST);
			$quest_status = $db->getResult();

			for($n = 0; $n < count($quests); $n++)
			{
				for($i = 0; $i < count($quest_status); $i++)
				{
					if($quest_status[$i]['entry'] == $quests[$n]['entry'])
					{
						$quests[$n]['status'] = $quest_status[$i]['status'];
					}
				}
			}
		}

		$total_quests = 0;
		$total_subzones = 0;
		$total_tested = 0;
		for($i = 0; $i < count($this->zones_array); $i++)
		{
			$quests_array = array();
			for($n = 0; $n < count($quests); $n++)
			{
				if($this->zones_array[$i]['id'] == $quests[$n]['zoneorsort'])
				{
					$quests_array[] = array('entry' => $quests[$n]['entry'], 'status' => $quests[$n]['status']);
				}else
				{
					for($a = 0; $a < count($this->zones_array[$i]['subzones']); $a++)
					{
						if($this->zones_array[$i]['subzones'][$a] == $quests[$n]['zoneorsort'])
						{
							$quests_array[] = array('entry' => $quests[$n]['entry'], 'status' => $quests[$n]['status']);
						}
					}
				}
			}
			$this->zones_array[$i]['quests'] = $quests_array;
			$this->zones_array[$i]['numQuests'] = count($this->zones_array[$i]['quests']);

			$progress = 0;
			for($n = 0; $n < count($this->zones_array[$i]['quests']); $n++)
			{
				if($this->zones_array[$i]['quests'][$n]['status'] != UNTESTED && $this->zones_array[$i]['quests'][$n]['status'] != NEEDS_MORE_TESTING)
					$progress++;
			}

			$this->zones_array[$i]['questProgTested'] = $progress;

			$total_quests += $this->zones_array[$i]['numQuests'];
			$total_subzones += $this->zones_array[$i]['numSubZones'];
			$total_tested += $progress;

			if($this->zones_array[$i]['numQuests'] > 0)
			{
				if($progress > 0)
					$this->zones_array[$i]['questProgPct'] = round((($progress) / ($this->zones_array[$i]['numQuests'])) * (100), 0, PHP_ROUND_HALF_DOWN);
				else
					$this->zones_array[$i]['questProgPct'] = 0;
			}else
			{
				$this->zones_array[$i]['questProgPct'] = 100;
			}
		}

		$zones_total['numQuests'] = $total_quests;
		$zones_total['numSubZones'] = $total_subzones;
		if($zones_total['numQuests'] > 0)
			$zones_total['questProgPct'] = round((($total_tested) / ($total_quests)) * (100), 0, PHP_ROUND_HALF_DOWN);
		else
			$zones_total['questProgPct'] = 100;

		$this->zones_list = $this->zones_array;
		$this->zones_total = $zones_total;
		
		/*

		if($custom = true)
		{
			if($continent == 0) // eastern kingdoms
				$zones = array(33, 4, 41, 8, 10, 40, 44, 12, 46, 51, 3, 1, 38, 11, 45, 267, 47, 130, 36, 85, 139, 28);
			else // kalimdor
				$zones = array(440, 490, 1377, 400, 357, 15, 215, 405, 17, 406, 14, 331, 16, 15, 361, 148, 493, 618);

			$where = implode(' OR `id`=', $zones);
		}

		// get all zones
		$db = new Database();
		$db->select('dbh1', 'areatable', 'id, zoneorsort', '`id`='.$where);
		$result = $db->getResult();
		$numResults = $db->numResults;

		for($i = 0; $i < count($result); $i++)
		{
			//$quest = new Quest;

			// get number of sub-zones
			$db->select('dbh1', 'areatable', 'id', '`COL 3`='.$result[$i]['id']);
			$subzones = $db->getResult();
			$numResultsSubZone = $db->numResults;
			$result[$i]['numSubZones'] = $numResultsSubZone;

			// get number of quests in zones + sub-zones
			$where = implode(' OR `zoneorsort`=', $this->getSubZones($result[$i]['id']));
			$db->select('dbh', 'quest_template', 'entry', '`zoneorsort`='.$where);
			$quests = $db->getResult();
			$numResultsQuests = $db->numResults;
			$result[$i]['numQuests'] = $numResultsQuests;
			/*
			// TODO: verify if progress is being calculated correctly
			$progress = 0;
			for($n = 0; $n < count($quests); $n++)
			{
				$quest->getStatus($quests[$n]['entry'], true);

				if($quest->status_raw != NEEDS_MORE_TESTING_TEXT && $quest->status_raw != UNTESTED)
					$progress++;
			}

			$result[$i]['questProgTested'] = $progress;

			if($progress > 0)
				$result[$i]['questProgPct'] = round((($progress) / ($result[$i]['numQuests'])) * (100), 0, PHP_ROUND_HALF_DOWN);
			else
				$result[$i]['questProgPct'] = 0;
		}*/
	}
}
?>