<?php

class Report
{
	public $numResults;
	public $report;
	public $fix_by;

	public $total_report_char = 0;

	public function getReports($rows = null, $entry = false, $type = false, $is_comment = false, $where = null, $order = null, $limit = null)
	{
		switch($rows)
		{
			case 1:
				$rows = '`id`';
			break;
			default:
				$rows = '*';
		}

		if($entry != false)
			$where = 'entry='.$entry.' AND type='.$type;

		$db = new Database('dbh1');
		$db->select('dbh1', 'testing_reports', $rows, $where, $order, null, $limit);
		$result = $db->getResult();
		$numResults = $db->numResults;

		if($numResults >= 1)
		{
			if($is_comment == true)
			{
				for($i = 0; $i < count($result); $i++)
				{
					$this->total_report_char += strlen($result[$i]['comment']);

					switch($result[$i]['type'])
					{
						case ITEM:
							$result[$i]['type_name'] = ITEM_NAME;
						break;
						case NPC:
							$result[$i]['type_name'] = NPC_NAME;
						break;
						case _OBJECT:
							$result[$i]['type_name'] = _OBJECT_NAME;
						break;
						case SPELL:
							$result[$i]['type_name'] = SPELL_NAME;
						break;
						case QUEST:
							$result[$i]['type_name'] = QUEST_NAME;
						break;
					}

					$result[$i]['comment'] = nl2br(htmlentities($result[$i]['comment']));

					if(empty($result[$i]['comment']))
						$result[$i]['comment'] = 'No additional information.';

					switch($result[$i]['status'])
					{
						// GENERAL
						case FIXED:
							$result[$i]['status'] = '<span class="tag tag8">'.FIXED_TEXT.'</span>';
						break;
						case DISCUSSION:
							$result[$i]['status'] = 'Discussion';
						break;
						case INCORRECT_ASSESSMENT:
							$result[$i]['status'] = INCORRECT_ASSESSMENT_TEXT;
						break;
						
						// ITEM
						case ITEM_NOT_WORKING_AS_INTENDED:
							$result[$i]['status'] = '<span class="tag tag2">'.NOT_WORKING_AS_INTENDED.'</span>';
						break;
						case ITEM_NO_BUGS_FOUND:
							$result[$i]['status'] = '<span class="tag tag4">'.NO_BUGS_FOUND.'</span>';
						break;
						case ITEM_WORKING_PERFECTLY:
							$result[$i]['status'] = '<span class="tag tag8">'.WORKING_PERFECTLY.'</span>';
						break;
						case ITEM_OBSOLETE:
							$result[$i]['status'] = '<span class="tag tag2">'.OBSOLETE.'</span>';
						break;
						
						// SPELL
						case SPELL_NOT_WORKING_AS_INTENDED:
							$result[$i]['status'] = '<span class="tag tag2">'.NOT_WORKING_AS_INTENDED.'</span>';
						break;
						case SPELL_NO_BUGS_FOUND:
							$result[$i]['status'] = '<span class="tag tag4">'.NO_BUGS_FOUND.'</span>';
						break;
						case SPELL_OBSOLETE:
							$result[$i]['status'] = '<span class="tag tag2">'.OBSOLETE.'</span>';
						break;

						// OBJECT
						case OBJECT_NOT_WORKING_AS_INTENDED:
							$result[$i]['status'] = '<span class="tag tag2">'.NOT_WORKING_AS_INTENDED.'</span>';
						break;
						case OBJECT_NO_BUGS_FOUND:
							$result[$i]['status'] = '<span class="tag tag4">'.NO_BUGS_FOUND.'</span>';
						break;
						case OBJECT_WORKING_PERFECTLY:
							$result[$i]['status'] = '<span class="tag tag8">'.WORKING_PERFECTLY.'</span>';
						break;
						case OBJECT_OBSOLETE:
							$result[$i]['status'] = '<span class="tag tag2">'.OBSOLETE.'</span>';
						break;

						// NPC
						case NPC_NOT_WORKING_AS_INTENDED:
							$result[$i]['status'] = '<span class="tag tag2">'.NOT_WORKING_AS_INTENDED.'</span>';
						break;
						case NPC_NO_BUGS_FOUND:
							$result[$i]['status'] = '<span class="tag tag4">'.NO_BUGS_FOUND.'</span>';
						break;
						case NPC_WORKING_PERFECTLY:
							$result[$i]['status'] = '<span class="tag tag8">'.WORKING_PERFECTLY.'</span>';
						break;
						case NPC_OBSOLETE:
							$result[$i]['status'] = '<span class="tag tag2">'.OBSOLETE.'</span>';
						break;

						// QUEST
						case QUEST_BUGS_FOUND:
							$result[$i]['status'] = '<span class="tag tag2">'.BUGS_FOUND.'</span>';
						break;
						case QUEST_NOT_COMPLETABLE:
							$result[$i]['status'] = '<span class="tag tag2">'.NOT_COMPLETABLE.'</span>';
						break;
						case QUEST_COMPLETABLE_NO_BUGS_FOUND:
							$result[$i]['status'] = '<span class="tag tag4">'.COMPLETABLE_NO_BUGS_FOUND.'</span>';
						break;
						case QUEST_OBSOLETE:
							$result[$i]['status'] = '<span class="tag tag2">'.OBSOLETE.'</span>';
						break;
						case QUEST_WORKING_PERFECTLY:
							$result[$i]['status'] = '<span class="tag tag8">'.WORKING_PERFECTLY.'</span>';
						break;

						default:
							$result[$i]['status'] = 'ERROR: UNKNOWN STATUS';
					}
				}
			}
 			$this->numResults = $numResults;
			return $this->report = $result;
		}else
		{
			$this->numResults = 0;
		}
	}

	public function insertReport($type = false)
	{
		if(@$_SESSION['user_logged_in'] === true && @$_SESSION['user_remote_ip'] === $_SERVER['REMOTE_ADDR'])
		{
			$user = new User;
			$redirect = new Redirect;
			
			if($type != false)
			{
				$user_id = @$_SESSION['user_id'];
				$comment = $_POST['report_comment'];
				$status = $_POST['report_status'];
				$read_ins = $_POST['read_ins'];
				$date = new Date;

				$_SESSION['report_comment'] = $comment;

				if(@$read_ins === 'true')
				{
					switch($type)
					{
						case ITEM:
						$entry = $_GET['item'];
						break;
						case NPC:
						$entry = $_GET['npc'];
						break;
						case _OBJECT:
						$entry = $_GET['object'];
						break;
						case SPELL:
						$entry = $_GET['spell'];
						break;
						case QUEST:
						$entry = $_GET['quest'];
						break;
					}

					if(is_numeric($entry))
					{
						$db = new Database('dbh1');
						$db->insert('dbh1', 'testing_reports', array($user_id, $comment, $status, $type, $entry, $date->getDate()),
							array('user_id', 'comment', 'status', 'type', 'entry', 'date'));

						unset($_SESSION['report_comment']);

						//$redirect->doRedirect('tracker/redirect.php?redirect='.$redirect->currentUrl(), 0, 1);
						exit($redirect->doRedirect('redirect.php?redirect='.$redirect->currentUrl(), 0, 1));
					}else
					{
						die('Report: Entry was not numeric!');
					}
				}else
				{
					//$redirect->doRedirect('tracker/redirect.php?redirect='.$redirect->currentUrl(), 0, null, 1);
					exit($redirect->doRedirect($redirect->currentUrl()));
				}
			}else
			{
				die('Error: A type must be specified!');
			}
		}
	}

	public function deleteReport()
	{
		if(@$_SESSION['user_logged_in'] === true && @$_SESSION['user_remote_ip'] === $_SERVER['REMOTE_ADDR'])
		{
			$user = new User;
			$redirect = new Redirect;

			$id = $_POST['report_id'];

			// only the user whom submitted a report can delete it, unless you're a GM
			$db = new Database('dbh1');
			$db->select('dbh1', 'testing_reports', '*', 'user_id='.$_SESSION['user_id'].' AND id='.$id);
			$result = $db->getResult();
			$numResults = $db->numResults;

			if($user->gmlevel() > 0 || $numResults >= 1)
			{
				$db->delete('dbh1', 'testing_reports', 'id='.$id);

				//$redirect->doRedirect('tracker/redirect.php?redirect='.$redirect->currentUrl(), 0, 2);
				exit($redirect->doRedirect('redirect.php?redirect='.$redirect->currentUrl(), 0, 2));
			}else
			{
				die('You are not authorized to do that! You will be murdered in 3..2..1..');
			}
		}else
		{
			die('You are not authorized to do that! You will be murdered in 3..2..1..');
		}
	}

	public function fixedReport()
	{
		if(@$_SESSION['user_logged_in'] === true && @$_SESSION['user_remote_ip'] === $_SERVER['REMOTE_ADDR'])
		{
			$user = new User;
			$redirect = new Redirect;
			$date = new Date;

			if($user->gmlevel() > 0)
			{
				$id = $_POST['report_id'];
				$fix_by = $_POST['report_fix_by'];

				if($fix_by == 0) // Fixed by X (X == logged in user)
				{
					$fix_by = @$_SESSION['user_id'];
				}elseif($fix_by == 1) // Fixed by unknown developer
				{
					$fix_by = -1;
				}else
				{
					die('The "Fix by" selection you made was invalid.');
				}

				$db = new Database('dbh1');
				$db->update('dbh1', 'testing_reports', array('fix_by_user_id' => $fix_by), array('id='.$id));
				$db->update('dbh1', 'testing_reports', array('fix_date' => $date->getDate()), array('id='.$id));
				$db->update('dbh1', 'testing_reports', array('status' => FIXED), array('id='.$id));

				//$redirect->doRedirect('tracker/redirect.php?redirect='.$redirect->currentUrl(), 0, 3);
				exit($redirect->doRedirect('redirect.php?redirect='.$redirect->currentUrl(), 0, 3));
			}else
			{
				die('You are not authorized to do that! You will be terminated in 3..2..1..');
			}
		}
	}
}

?>