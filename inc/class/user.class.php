<?php
class User
{
	public $user;
	public $numResults;

	public $gmlevel;
	public $username;

	public $characters;

	public function login()
	{
		if(isset($_POST['login_submit']))
		{
			$raw_username = $_POST['login_username'];
			$raw_password = $_POST['login_password'];

			// check if all form fields are filled out
			if(!empty($raw_username) AND !empty($raw_password))
			{
				$hashed_pass = strtoupper(sha1(strtoupper($raw_username.":".$raw_password))); // hash password and make it uppercase

				$where = '`username` LIKE "'.$raw_username.'" AND `sha_pass_hash` LIKE "'.$hashed_pass.'"';

				$db = new Database('dbh3');
				$db->select('dbh3','account', 'id, username, sha_pass_hash', ''.$where.'');
				$result = $db->getResult();
				$numResults = $db->numResults;

				if($numResults === 1)
				{
					foreach($result as $row)
					{
						// check if username and password from database matches post data (reason: extra security)
						if(mb_strtoupper($raw_username) == mb_strtoupper($row['username']) && mb_strtoupper($hashed_pass) == mb_strtoupper($row['sha_pass_hash']))
						{
							unset($_SESSION['user_logged_in']);
							unset($_SESSION['user_id']);

							$_SESSION['user_logged_in'] = true;
							$_SESSION['user_id'] = $row['id'];
							$_SESSION['user_remote_ip'] = $_SERVER['REMOTE_ADDR'];

							$redirect = new Redirect;
							die($redirect->doRedirect('redirect.php?redirect=index.php', 0, 4));
						}else
						{
							$redirect = new Redirect;
							die($redirect->doRedirect('redirect.php?redirect=login.php', 0, null, 8));
						}
					}
				}else
				{
					$redirect = new Redirect;
					die($redirect->doRedirect('redirect.php?redirect=login.php', 0, null, 8));
				}
			}else
			{
				$redirect = new Redirect;
				die($redirect->doRedirect('redirect.php?redirect=login.php', 0, null, 4));
			}
		}
	}

	public function logout()
	{
		session_destroy();

		$redirect = new Redirect;
		die($redirect->doRedirect('redirect.php?redirect=index.php', 0, 5));
	}

	public function signup()
	{
		if(isset($_POST['signup_submit']))
		{
			$raw_username = $_POST['signup_username'];
			$raw_password = $_POST['signup_password'];
			$raw_password_again = $_POST['signup_password_again'];

			if(!empty($raw_username) AND !empty($raw_password) AND !empty($raw_password_again)) // check if all form fields are filled out
			{
				if($raw_password === $raw_password_again) // check if passwords match each other
				{
					if(preg_match('/[^0-9A-Za-z]/',$raw_username) === 0) // only allow letters and numbers in username
					{
						$db = new Database('dbh3');
						$db->select('dbh3','account', 'id', '`username` LIKE "'.$raw_username.'"');
						$usernameTaken = $db->numResults;

						if($usernameTaken == 0)
						{
							if(strlen($raw_username) <= 16 && strlen($raw_password) <= 16)
							{
								$hashed_pass = strtoupper(sha1(strtoupper($raw_username.":".$raw_password))); // hash password and make it uppercase

								$db = new Database('dbh3');
								$db->insert('dbh3', 'account', array($raw_username, $hashed_pass),
								array('username', 'sha_pass_hash'));

								$redirect = new Redirect;
								die($redirect->doRedirect('redirect.php?redirect=signup.php', 0, 6));
							}else
							{
								$redirect = new Redirect;
								die($redirect->doRedirect('redirect.php?redirect=signup.php', 0, null, 9));
								die('Tom');
							}
						}else
						{
							$redirect = new Redirect;
							die($redirect->doRedirect('redirect.php?redirect=signup.php', 0, null, 7));
						}
					}else
					{
						$redirect = new Redirect;
						die($redirect->doRedirect('redirect.php?redirect=signup.php', 0, null, 6));
					}
				}else
				{
					$redirect = new Redirect;
					die($redirect->doRedirect('redirect.php?redirect=signup.php', 0, null, 5));
				}
			}else
			{
				$redirect = new Redirect;
				die($redirect->doRedirect('redirect.php?redirect=signup.php', 0, null, 4));
			}
		}
	}

	public function getCharacters($account_id = null)
	{
		$wow = new WoW;

		if($account_id != null)
			$where = '`account`='.$account_id;
		else
			$where = null;

		$db = new Database('dbh4');
		$db->select('dbh4','characters', 'guid, account, name, race, class, gender, level, online', ''.$where.'');
		$characters['characters'] = $db->getResult();
		$characters['numChars'] = $db->numResults;

		for($i = 0; $i < count($characters['characters']); $i++)
		{
			// online
			if($characters['characters'][$i]['online'] == 0)
			{
				$characters['characters'][$i]['online'] = '<span class="offline">Offline</span>';
			}else
			{
				$characters['characters'][$i]['online'] = '<span class="online">Online</span>';
			}

			// race
			$characters['characters'][$i]['race'] = $wow->getRace($characters['characters'][$i]['race']);
			// class
			$characters['characters'][$i]['class'] = $wow->getClass($characters['characters'][$i]['class']);
			// gender
			$characters['characters'][$i]['gender'] = $wow->getGender($characters['characters'][$i]['gender']);
		}
		$this->characters = $characters;
	}

	public function getUser($where = null, $rows = '*', $join = null, $limit = null)
	{
		$db = new Database('dbh3');
		$db->select('dbh3','account', 'id, username, active_realm_id', ''.$where.'', null, ''.$join.'', $limit);
		$result = $db->getResult();
		$numResults = $db->numResults;
		if($numResults >= 1)
		{
			for($i = 0; $i < count($result); $i++)
			{	
				if($result[$i]['active_realm_id'] == 0)
				{
					$result[$i]['online'] = '<span class="offline">Offline (in-game)</span>';
				}else
				{
					$result[$i]['online'] = '<span class="online">Online (in-game)</span>';
				}
			}

			$this->numResults = $numResults;
			return $this->user = $result;
		}
		
		return false;
	}

	// OLD getUser used for forum accounts

	/*
	public function getUser($where = null, $rows = '*', $join = null, $limit = null)
	{
		$db = new Database();
		$db->select('dbh2','users', 'id, username, signature, location', ''.$where.'', null, ''.$join.'', $limit);
		$result = $db->getResult();
		$numResults = $db->numResults;
		if($numResults >= 1)
		{
			for($i = 0; $i < count($result); $i++)
			{	
				$result[$i]['signature'] = nl2br(htmlentities($result[$i]['signature']));
				
				if($result[$i]['location'] == '')
					$result[$i]['location'] = '<span class="italic">Not specified</span>';
			}

			$this->numResults = $numResults;
			return $this->user = $result;
		}
		
		return false;
	}
	*/

	public function gmlevel()
	{
		if(!empty($_SESSION['user_id']))
		{
			$db = new Database('dbh3');
			$db->select('dbh3','account', 'gmlevel', '`id`='.$_SESSION['user_id']);
			$result = $db->getResult();
			$numResults = $db->numResults;
			if($numResults > 0)
			{
				for($i = 0; $i < count($result); $i++)
				{
					return $this->gmlevel = $result[$i]['gmlevel'];
				}
			}
		}
		return 0;
	}

	public function id2nick($id)
	{
	    if ($id > 0)
	    {
			$db = new Database('dbh3');
			$db->select('dbh3','account', 'username', '`id`='.$id);
			$result = $db->getResult();
			$numResults = $db->numResults;
			if($numResults > 0)
			{
				for($i = 0; $i < count($result); $i++)
				{
					return $this->username = $result[$i]['username'];
				}
			}
		}
		return "Unknown";
	}

}
?>