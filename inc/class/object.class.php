<?php

class Object
{
	public $object;
	public $object_guid;
	public $status;
	public $numResults = 0;

	public function getObjects($list = false, $where = null, $join = null, $limit = null)
	{
		if($list == true)
			$rows = '`entry`, `name`';
		else
			$rows = '*';

		$db = new Database('dbh');
		$db->select('dbh','gameobject_template', $rows, ''.$where.'', null, ''.$join.'', $limit);
		$result = $db->getResult();
		$numResults = $db->numResults;
		if($numResults >= 1)
		{
			$this->numResults = $numResults;
			return $this->object = $result;
		}else
		{
			return false;
		}
	}

	public function getStatus($entry = false)
	{
		$db = new Database('dbh1');
		$db->select('dbh1','testing_entry_status', 'entry, status', '(`entry`='.$entry.') AND `type`='._OBJECT);
		$result = $db->getResult();
		$numResults = $db->numResults;

		switch(@$result[0]['status'])
		{
			case OBJECT_NOT_WORKING_AS_INTENDED:
				if(@$text)
					$status = NOT_WORKING_AS_INTENDED;
				else
					$status = '<span class="tag tag2">'.NOT_WORKING_AS_INTENDED.'</span>';
			break;
			case OBJECT_OBSOLETE:
				if(@$text)
					$status = OBSOLETE;
				else
					$status = '<span class="tag tag2">'.OBSOLETE.'</span>';
			break;
			case OBJECT_WORKING_PERFECTLY:
				if(@$text)
					$status = WORKING_PERFECTLY;
				else
					$status = '<span class="tag tag8">'.WORKING_PERFECTLY.'</span>';
			break;
			case OBJECT_NO_BUGS_FOUND:
				if(@$text)
					$status = NO_BUGS_FOUND;
				else
					$status = '<span class="tag tag4">'.NO_BUGS_FOUND.'</span>';
			break;
			case NEEDS_MORE_TESTING:
				if(@$text)
					$status = NEEDS_MORE_TESTING_TEXT;
				else
					$status = '<span class="tag tag_nmt">'.NEEDS_MORE_TESTING_TEXT.'</span>';
			break;
			default:
				if(@$text)
					$status = UNTESTED_TEXT;
				else
					$status = '<span class="tag tag0">'.UNTESTED_TEXT.'</span>';
		}

		return $this->status = $status;

		/*$report = new Report;
		$report->getReports(null, $entry, _OBJECT);

		if($report->numResults > 0)
		{
			$OBJECT_NOT_WORKING_AS_INTENDED = 0;
			$OBJECT_NO_BUGS_FOUND = 0;
			$OBJECT_WORKING_PERFECTLY = 0;
			$OBJECT_OBSOLETE = 0;

			foreach($report->report as $report):
				switch($report['status'])
				{
					case OBJECT_NOT_WORKING_AS_INTENDED:
						$OBJECT_NOT_WORKING_AS_INTENDED++;
					break;
					case OBJECT_OBSOLETE:
						$OBJECT_OBSOLETE++;
					break;
					case OBJECT_NO_BUGS_FOUND:
						$OBJECT_NO_BUGS_FOUND++;
					break;
					case OBJECT_WORKING_PERFECTLY:
						$OBJECT_WORKING_PERFECTLY++;
					break;
				}
			endforeach;

			if(@$OBJECT_NOT_WORKING_AS_INTENDED >= OBJECT_NOT_WORKING_AS_INTENDED)
				return $this->status = '<span class="tag tag2">'.NOT_WORKING_AS_INTENDED.'</span>';
			elseif(@$OBJECT_WORKING_PERFECTLY >= OBJECT_WORKING_PERFECTLY)
				return $this->status = '<span class="tag tag8">'.WORKING_PERFECTLY.'</span>';
			elseif(@$OBJECT_NO_BUGS_FOUND >= OBJECT_NO_BUGS_FOUND)
				return $this->status = '<span class="tag tag4">'.NO_BUGS_FOUND.'</span>';
			elseif(@$OBJECT_OBSOLETE >= OBJECT_OBSOLETE)
				return $this->status = '<span class="tag tag2">'.OBSOLETE.'</span>';
			elseif(@$OBJECT_NOT_WORKING_AS_INTENDED > 0 || @$OBJECT_NO_BUGS_FOUND > 0  || @$OBJECT_WORKING_PERFECTLY > 0 || @$OBJECT_OBSOLETE > 0)
				return $this->status = '<span class="tag tag_nmt">'.NEEDS_MORE_TESTING.'</span>';
			else
				return $this->status = '<span class="tag tag0">'.UNTESTED.'</span>';
		}else
		{
			return $this->status = '<span class="tag tag0">'.UNTESTED.'</span>';
		}*/
	}

	public function getGuids($entry, $guid = null)
	{
		if($guid != null)
			$where = 'guid='.$guid;
		else
			$where = 'id='.$entry;

		$db = new Database('dbh');
		$db->select('dbh','gameobject', '*', ''.$where.'');
		$result = $db->getResult();
		$numResults = $db->numResults;
		if($numResults > 0)
		{
			for($i = 0; $i < count($result); $i++)
			{
				switch($result[$i]['map'])
				{
					case 0:
						$result[$i]['map'] = $result[$i]['map'].' (Kalimdor)';
					break;
					case 1:
						$result[$i]['map'] = $result[$i]['map'].' (Eastern Kingdoms)';
					break;
				}
			}

			$this->numResults = $numResults;
			return $this->object_guid = $result;
		}
	}
}

?>