<?php
class Statistics
{
	public $statistics;

	public function getStatisticsInDb()
	{
		$db = new Database('dbh1');
		$db->select('dbh1','statistics', '*');
		$statistics = $db->getResult();

		for($i = 0; $i < count($statistics); $i++)
		{
			$array[$statistics[$i]['name']] = $statistics[$i]['value'];
		}

		$this->statistics = $array;
	}

	public function getNumOnlinePlayers()
	{
		$db = new Database('dbh3');
		$db->select('dbh3','account', 'id', '`active_realm_id` <> 0');
		$numResults = $db->numResults;

		$this->statistics['numOnlinePlayers'] = $numResults;
	}
}
?>