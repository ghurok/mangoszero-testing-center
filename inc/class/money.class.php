<?php
	class Money
	{
		public $gold;
		public $silver;
		public $copper;
		public $money;

		public function getFormatMoney($RewOrReqMoney)
		{
			$money = $RewOrReqMoney;
			
			switch(true)
			{
				case $money < 100 && $money > 0:
					$copper = $money;
				break;
				case $money >= 100 && $money < 10000:
					$silver = (int)($money / 100);
					$copper = ($money) - ($silver * 100);
				break;
				case $money >= 10000:
					$gold = (int)($money / 10000);
					$silver = (int)((($money) - ($gold * 10000)) / 100);
					$copper = ($money) - (($gold * 10000) + ($silver * 100));
				break;
				default:
					return $this->money = '<span class="moneycopper">0</span>'.' ';
			}
			
			if(@$gold > 0)
			{
				$gold = '<span class="moneygold">'.$gold.'</span>'.' ';
			}else
			{
				$gold = NULL;
			}
			
			if(@$silver > 0)
			{
				$silver = '<span class="moneysilver">'.$silver.'</span>'.' ';
			}else
			{
				$silver = NULL;
			}
			
			if(@$copper > 0)
			{
				$copper = '<span class="moneycopper">'.$copper.'</span>'.' ';
			}else
			{
				$copper = NULL;
			}

			return $this->money = $gold.$silver.$copper;
		}
	}
?>