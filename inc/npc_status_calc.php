<?php

$NPC_NOT_WORKING_AS_INTENDED = 0;
$NPC_NO_BUGS_FOUND = 0;
$NPC_WORKING_PERFECTLY = 0;
$NPC_OBSOLETE = 0;

for($n = 0; $n < count($count_loop[$i]['status']); $n++)
{
	switch($loop_switch[$i]['status'][$n])
	{
		case NPC_NOT_WORKING_AS_INTENDED:
			$NPC_NOT_WORKING_AS_INTENDED++;
		break;
		case NPC_OBSOLETE:
			$NPC_OBSOLETE++;
		break;
		case NPC_NO_BUGS_FOUND:
			$NPC_NO_BUGS_FOUND++;
		break;
		case NPC_WORKING_PERFECTLY:
			$NPC_WORKING_PERFECTLY++;
		break;
	}
}

switch(true)
{
	case @$NPC_NOT_WORKING_AS_INTENDED >= NPC_NOT_WORKING_AS_INTENDED_MIN:
			$status = NPC_NOT_WORKING_AS_INTENDED;
	break;
	case @$NPC_OBSOLETE >= NPC_OBSOLETE_MIN:
			$status = NPC_OBSOLETE;
	break;
	case @$NPC_WORKING_PERFECTLY >= NPC_WORKING_PERFECTLY_MIN:
			$status = NPC_WORKING_PERFECTLY;
	break;
	case @$NPC_NO_BUGS_FOUND >= NPC_NO_BUGS_FOUND_MIN:
			$status = NPC_NO_BUGS_FOUND;
	break;
	case @$NPC_NOT_WORKING_AS_INTENDED > 0 || @$NPC_OBSOLETE > 0 || @$NPC_WORKING_PERFECTLY > 0 || @$NPC_NO_BUGS_FOUND > 0:
			$status = NEEDS_MORE_TESTING;
	break;
	default:
			$status = UNTESTED;
}
?>