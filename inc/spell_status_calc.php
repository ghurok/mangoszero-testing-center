<?php

$SPELL_NOT_WORKING_AS_INTENDED = 0;
$SPELL_NO_BUGS_FOUND = 0;
$SPELL_OBSOLETE = 0;

for($n = 0; $n < count($count_loop[$i]['status']); $n++)
{
	switch($loop_switch[$i]['status'][$n])
	{
		case SPELL_NOT_WORKING_AS_INTENDED:
			$SPELL_NOT_WORKING_AS_INTENDED++;
		break;
		case SPELL_OBSOLETE:
			$SPELL_OBSOLETE++;
		break;
		case SPELL_NO_BUGS_FOUND:
			$SPELL_NO_BUGS_FOUND++;
		break;
	}
}

switch(true)
{
	case @$SPELL_NOT_WORKING_AS_INTENDED >= SPELL_NOT_WORKING_AS_INTENDED_MIN:
			$status = SPELL_NOT_WORKING_AS_INTENDED;
	break;
	case @$SPELL_OBSOLETE >= SPELL_OBSOLETE_MIN:
			$status = SPELL_OBSOLETE;
	break;
	case @$SPELL_NO_BUGS_FOUND >= SPELL_NO_BUGS_FOUND_MIN:
			$status = SPELL_NO_BUGS_FOUND;
	break;
	case @$SPELL_NOT_WORKING_AS_INTENDED > 0 || @$SPELL_OBSOLETE > 0 || @$SPELL_NO_BUGS_FOUND > 0:
			$status = NEEDS_MORE_TESTING;
	break;
	default:
			$status = UNTESTED;
}
?>