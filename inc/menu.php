<div class="menu global-menu">
	<ul>
		<li><a href="http://project-silverpine.com">Home</a></li>
		<li><a href="http://project-silverpine.com/aboutus">About Us</a></li>
		<li><a href="http://project-silverpine.com/forums">Forums</a></li>
		<li><a href="http://project-silverpine.com/register">Account</a></li>
		<li><a href="http://project-silverpine.com/database">Testing Center</a></li>
		<li><a href="http://project-silverpine.com/patchnotes">Patch Notes</a></li>
		<div id="browse" onmouseover="showMenu(1)" onmouseout="startTimer()"><li><a href="http://project-silverpine.com/devwiki">Development</a></li></div>
		<li><a href="http://project-silverpine.com/downloads">Downloads</a></li>
		<li><a href="http://project-silverpine.com/donate">Donate</a></li>
		<div class="clear"></div>
	</ul>
</div>
<div class="clear"></div>

<div class="menu">
	<ul class="menu-left">
		<li><a href="index.php">Home</a></li>
		<li><a href="phases.php" class="menu-bold">Testing Phases</a></li>
		<!-- <li><a href="profiles.php">Profiles</a></li> !-->
		<!-- <li><a href="statistics.php">Statistics</a></li> !-->
		<!-- <li><a href="achievements.php">Achievements</a></li> !-->
		<!-- <li><a href="leaderboards.php">Leaderboards</a></li> !-->
		<div class="clear"></div>
	</ul>
	<ul class="menu-right">
		<?php if(@$_SESSION['user_logged_in'] === true): ?>
			<li><a href="profile.php?user=<?php echo @$_SESSION['user_id']; ?>">My Profile</a></li>
			<li><a href="logout.php">Log out</a></li>
		<?php endif; ?>

		<?php if(@$_SESSION['user_logged_in'] !== true): ?>
			<li><a href="signup.php">Sign up</a></li>
			<li><a href="login.php">Log in</a></li>
		<?php endif; ?>
	</ul>
	<div class="clear"></div>
	<ul class="menu-left">
		<li><a href="quests.php">Quests</a></li>
		<li><a href="items.php">Items</a></li>
		<li><a href="npcs.php">NPCs</a></li>
		<li><a href="spells.php">Spells</a></li>
		<li><a href="objects.php">Objects</a></li>
		<div class="clear"></div>
	</ul>
	<ul class="menu-right">
		<li><a href="zones.php">Zones</a></li>
		<li><a href="classes.php">Classes</a></li>
		<li><a href="professions.php">Professions</a></li>
		<li><a href="instances.php">Instances</a></li>
		<li><a href="miscs.php">Misc</a></li>
	</ul>
	<div class="clear"></div>
</div>
<div class="clear"></div>