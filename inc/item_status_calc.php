<?php

$ITEM_NOT_WORKING_AS_INTENDED = 0;
$ITEM_NO_BUGS_FOUND = 0;
$ITEM_WORKING_PERFECTLY = 0;
$ITEM_OBSOLETE = 0;

for($n = 0; $n < count($count_loop[$i]['status']); $n++)
{
	switch($loop_switch[$i]['status'][$n])
	{
		case ITEM_NOT_WORKING_AS_INTENDED:
			$ITEM_NOT_WORKING_AS_INTENDED++;
		break;
		case ITEM_OBSOLETE:
			$ITEM_OBSOLETE++;
		break;
		case ITEM_NO_BUGS_FOUND:
			$ITEM_NO_BUGS_FOUND++;
		break;
		case ITEM_WORKING_PERFECTLY:
			$ITEM_WORKING_PERFECTLY++;
		break;
	}
}

switch(true)
{
	case @$ITEM_NOT_WORKING_AS_INTENDED >= ITEM_NOT_WORKING_AS_INTENDED_MIN:
			$status = ITEM_NOT_WORKING_AS_INTENDED;
	break;
	case @$ITEM_OBSOLETE >= ITEM_OBSOLETE_MIN:
			$status = ITEM_OBSOLETE;
	break;
	case @$ITEM_WORKING_PERFECTLY >= ITEM_WORKING_PERFECTLY_MIN:
			$status = ITEM_WORKING_PERFECTLY;
	break;
	case @$ITEM_NO_BUGS_FOUND >= ITEM_NO_BUGS_FOUND_MIN:
			$status = ITEM_NO_BUGS_FOUND;
	break;
	case @$ITEM_NOT_WORKING_AS_INTENDED > 0 || @$ITEM_OBSOLETE > 0 || @$ITEM_WORKING_PERFECTLY > 0 || @$ITEM_NO_BUGS_FOUND > 0:
			$status = NEEDS_MORE_TESTING;
	break;
	default:
			$status = UNTESTED;
}
?>