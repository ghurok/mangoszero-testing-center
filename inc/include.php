<?php
	//include 'inc/class/trackerSessionHandler.class.php';

	/*$trackerSessionHandler = new TrackerSessionHandler();
	session_set_save_handler($trackerSessionHandler, true);*/
	session_start();

	if($_SERVER['SERVER_NAME'] == '127.0.0.1')
		include 'testing_center_conf/config.php';
	else
		include '../../website/testing_center_conf/config.php';
	
	include 'inc/class/redirect.class.php';
	include 'inc/class/report.class.php';
	include 'inc/class/database.class.php';
	include 'inc/class/item.class.php';
	include 'inc/class/spell.class.php';
	include 'inc/class/quest.class.php';
	include 'inc/class/object.class.php';
	include 'inc/class/npc.class.php';
	include 'inc/class/paginator.class.php';
	include 'inc/class/date.class.php';
	include 'inc/class/search.class.php';
	include 'inc/class/money.class.php';
	include 'inc/class/user.class.php';
	include 'inc/class/breadcrumb.class.php';
	include 'inc/class/area.class.php';
	include 'inc/class/phase.class.php';
	include 'inc/class/questsort.class.php';
	include 'inc/class/wow.class.php';
	include 'inc/class/faction.class.php';
	include 'inc/class/classes.class.php';

	include 'inc/class/cronjob.class.php';
	include 'inc/class/statistics.class.php';

	if(isset($_POST['search']))
	{
	$search = new Search;
	$search->search();
	}

	$user = new user;
	$breadcrumb = new Breadcrumb;
?>