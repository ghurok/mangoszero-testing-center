<?php
	include 'inc/include.php';

	$item = new Item;
	$report = new Report;

	$BuyPrice = new Money;
	$SellPrice = new Money;

	if(isset($_POST['submit_report']))
		$report->insertReport(ITEM);

	if(isset($_POST['delete_report']))
		$report->deleteReport();

	if(isset($_POST['fixed_report']))
		$report->fixedReport();

	if(!empty($_GET['item']) && is_numeric($_GET['item']))
	{
		$item->getItems($list = false, 'entry='.$_GET['item']);
		$item->getStatus($_GET['item']);
		$report->getReports(null, $_GET['item'], ITEM, true);
	?>
	<head>
		<title>Item: <?php echo $item->item[0]['name']; ?> (<?php echo $item->item[0]['entry']; ?>)</title>
		<link rel="stylesheet" type="text/css" href="css/tracker.css">
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/tabs.js"></script>
	</head>
	<?php
	}else
	{
		//$t_redirect->doRedirect('tracker/redirect.php?redirect='.'items', 0, null, 2);
		//exit($t_redirect->doRedirect($t_redirect->currentUrl()));
	}
?>
<div id="t_wrapper">
	<?php include 'inc/menu.php'; ?>
	<?php include 'inc/precontents.php'; ?>
	<div id="content-wrapper">
		<div id="main-content">
			<div id="content-left">
				<h1><span style="color: <?php echo $item->item[0]['item_color']; ?>;"><?php echo $item->item[0]['name']; ?> (<?php echo $item->item[0]['entry']; ?>)</span> - <?php echo $item->status; ?></h1>
				<div id="gen-info-wrapper">
					<h3>General Information</h3>
					<table>
					<tr><td>Buy Price:</td> <td><?php echo $BuyPrice->getFormatMoney($item->item[0]['BuyPrice']); ?></td></tr>
					<tr><td>Sell Price: <td><?php echo $SellPrice->getFormatMoney($item->item[0]['SellPrice']); ?></td></tr>
					<tr><td>Bonding: <td><?php echo $item->item[0]['bonding']; ?></td></tr>
					<tr><td>Class: <td><?php echo $item->item[0]['class']; ?></td></tr>
					<tr><td>Item Level: <td><?php echo $item->item[0]['ItemLevel']; ?></td></tr>
					<tr><td>Req. Level: <td><?php echo $item->item[0]['RequiredLevel']; ?></td></tr>
					</table>
				</div>
			</div>
		</div>
		<div id="related">
			<div id="tabs-container">
				<ul>
					<li><a href="#comments" class="showSingle" data-target="1">Reports (<?php echo $report->numResults; ?>)</a></li>
					<li><a href="#links" class="showSingle" data-target="2">Links</a></li>
				</ul>
				<div class="clear"></div>
			</div>
			<div id="tabs-content">
				<div id="tabs-content-1" class="targetDiv">
				<div id="left-content">
					<?php if($report->numResults >= 1): ?>
						<?php foreach($report->report as $row): ?>
								<article id="comment">
								<?php if($user->gmlevel() > 0): ?>
								<div class="h-comment-admin">
									<span class="h-comment-left">
										<form class="admin_panel_delete" method="post">
											<input type="hidden" name="report_id" value="<?php echo $row['id']; ?>" />
											<input type="submit" name="delete_report" class="delete_report" value="" />
										</form>
									</span>
									<span class="h-comment-right">
										<form class="admin_panel_fixed" method="post">
											<select name="report_fix_by" class="report_fix_by">
												<option value="0">Fix by <?php echo $user->id2nick(@$_SESSION['user_id']); ?></option>
												<option value="1">Fix by someone else</option>
											</select>
											<input type="hidden" name="report_id" value="<?php echo $row['id']; ?>" />
											<input type="submit" name="fixed_report" class="fixed_report" value="" />
										</form>
									</span>
								</div>
								<?php elseif($row['user_id'] == @$_SESSION['user_id'] && @$_SESSION['user_id'] > 0 && $row['user_id'] > 0): ?>
								<div class="h-comment-admin">
									<span class="h-comment-left">
										<form class="admin_panel_delete" method="post">
											<input type="hidden" name="report_id" value="<?php echo $row['id']; ?>" />
											<input type="submit" name="delete_report" class="delete_report" value="" />
										</form>
									</span>
								</div>
								<?php endif; ?>
								<div class="h-comment">
									<span class="h-comment-right">
										<span class="bold"><?php echo $row['status']; ?></span>
									</span>
									<?php if($row['fix_by_user_id'] != NULL): ?>
										<span class="h-comment-left">
											<?php if($row['fix_by_user_id'] != -1): ?>
												<span>Fixed by <span class="bold"><a href="profile.php?user=<?php echo $row['fix_by_user_id']; ?>"><?php echo $user->id2nick($row['fix_by_user_id']); ?></a></span></span>
											<?php else: ?>
												<span>Fixed by unknown developer</span>
											<?php endif; ?>
										</span>
									<?php endif; ?>
								</div>
								<section>
									<p class="comment"><?php echo $row['comment']; ?></p>
									<footer>
										<span class="f-comment-left">
											<p class="reported_by">Posted by <span class="bold"><a href="profile.php?user=<?php echo $row['user_id']; ?>"><?php echo $user->id2nick($row['user_id']); ?></a></span> on <span class="bold"><?php echo $row['date']; ?></span></p>
										</span>
										<span class="f-comment-right">
											<p><span class="db_rev_comment"><?php echo $row['db_rev']; ?></span></p>
										</span>
										<div class="clear"></div>
									</footer>
								</section>
								</article>
						<?php endforeach; ?>
					<?php else: ?>
						<p id="no_reports">There are no reports.</p>
					<?php endif; ?>
					<?php if(isset($_SESSION["user_id"])): ?>
					<form action="#" method="post" name="report_form" id="report_form">
						<p>Status:</p>
						<select name="report_status">
							<option value="<?php echo DISCUSSION; ?>">Discussion</option>
							<option value="<?php echo ITEM_NOT_WORKING_AS_INTENDED; ?>"><?php echo NOT_WORKING_AS_INTENDED; ?></option>
							<option value="<?php echo ITEM_NO_BUGS_FOUND; ?>"><?php echo NO_BUGS_FOUND; ?></option>
							<option value="<?php echo ITEM_WORKING_PERFECTLY; ?>"><?php echo WORKING_PERFECTLY; ?></option>
							<option value="<?php echo ITEM_OBSOLETE; ?>"><?php echo OBSOLETE; ?></option>
						</select>
						<p>Comment:</p>
						<textarea name="report_comment"><?php echo @$_SESSION['report_comment']; ?></textarea><br />
						<input type="checkbox" name="read_ins" id="read_ins" value="true"> I have read the instructions!<br />
						<input type="submit" name="submit_report" value="Submit" />
					</form>
					<?php else: ?>
						<p>Log in to make a report.</p>
					<?php endif; ?>
				</div>
				<div id="right-content">
				<div id="instructions">
					<h2>Instructions (read before posting)</h2>
					<section>
					<p>
					Do not report drop rate/drop location bugs (except quest items drops, which should be reported at the relevant quest entry instead).
					If you don't know where to report a bug, use your own judgment. We'd rather get a bug report in the wrong category than no bug report at all.
					</p>
					</section>
					<section>
					<h4 class="status_white">Discussion</h4>
					<p>Not really a status. This is used for when you want to discuss something.</p>
					</section>
					<section>
					<span class="tag tag2">Not working as intended</span>
					<p>The item is bugged in some way (stats, stack size, price and so forth).</p>
					</section>
					<section>
					<span class="tag tag4">No bugs found</span>
					<p>A basic examination (used a potion, compared a weapon or armor to a reliable pre-tbc source and so forth) revealed no bugs.</p>
					</section>
					<section>
					<span class="tag tag2">Obsolete</span>
					<p>The item is not available in-game AND shouldn't be available in-game. If it is available, report it as not working as intended.</p>
					</section>
					<section>
					<span class="tag tag8">Working Perfectly</span>
					<ul>
					<p class="bold">Compare the following:</p>
					<li>Stats</li>
					<li>iLevel</li>
					<li>Buy and Sell Price</li>
					<li>Maximum Stack Size</li>
					<li>Effect when used (potion recovery, buff provided)</li>
					<li>Cooldown</li>
					<li>Stats when examined and equipped</li>
					<li>Proc chance</li>
					<li>PPM or PPH?</li>
					<p class="bold">to a reliable pre-tbc source. Links are required.</p>
					</ul>
					</section>
				</div>
				</div>
				</div>
				<div id="tabs-content-2" class="targetDiv">
					<ul id="list_links">
						<li><a href="http://web.archive.org/web/*/http://thottbot.com/i<?php echo $_GET['item']; ?>" target="_blank">Wayback Machine: Thottbot</a></li>
						<li><a href="http://web.archive.org/web/*/http://wow.allakhazam.com/db/item.html?witem=<?php echo $_GET['item']; ?>" target="_blank">Wayback Machine: Allakhazam</a></li>
						<li><a href="http://old.wowhead.com/item=<?php echo $_GET['item']; ?>" target="_blank">Wowhead (WoTLK)</a></li>
						<li><a href="http://wowd.org/items/<?php echo $_GET['item']; ?>.html" target="_blank">Wowd</a></li>
					</ul>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>
</div>