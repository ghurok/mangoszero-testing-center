<?php

include '/home/projects/website/testing_center_conf/config.php';
include '/home/projects/public_html/database/inc/class/database.class.php';
include '/home/projects/public_html/database/inc/class/graphData.class.php';

require_once ('../jpgraph.php');
require_once ('../jpgraph_bar.php');

include '../../inc/class/date.class.php';
$date = new Date;
$year = $date->getYear();

$graphData = new GraphData();

if(isset($_GET['user']))
	$graphData->getReportPerMonth($year, $_GET['user']);
else
	$graphData->getReportPerMonth($year);

//print_r($graphData->tick_num_month);
//print_r($graphData->tick_unnum_month);

//$datay=array(62,105,85,50,50,50,50,50,50,50,50,50);
//$datay=array($graphData->getReportPerMonth(2013));
$datay = $graphData->num_per_month;

// Create the graph. These two calls are always required
$graph = new Graph(900,350,'auto');
$graph->SetScale("textlin");

//$theme_class="DefaultTheme";
//$graph->SetTheme(new $theme_class());

// set major and minor tick positions manually
$graph->yaxis->SetTickPositions($graphData->tick_num_month);
$graph->SetBox(false);

//$graph->ygrid->SetColor('gray');
$graph->ygrid->SetFill(false);
$graph->xaxis->SetTickLabels(array('January','February','March','April','May','June','July','August','September','October','November','December'));
$graph->xaxis->SetColor('#CCCCCC');

$graph->yaxis->HideLine(false);
$graph->yaxis->HideTicks(false,false);
$graph->yaxis->SetColor('#CCCCCC');

$graph->SetColor('#0D0808');
$graph->SetMarginColor('#0D0808');
$graph->SetFrame(true,'#0D0808',2);

// Create the bar plots
$b1plot = new BarPlot($datay);

// ...and add it to the graPH
$graph->Add($b1plot);

$b1plot->value->Show();
$b1plot->SetColor("white");
$b1plot->value->SetColor('#CCCCCC');
$b1plot->SetFillGradient("#FFD100","#FFE159",GRAD_LEFT_REFLECTION);
$b1plot->SetWidth(45);
$b1plot->SetWeight(0);

$graph->title->Set("Reports submitted per month (".$year.")");
$graph->title->SetColor("#CCCCCC");

// Display the graph
$graph->Stroke();
?>