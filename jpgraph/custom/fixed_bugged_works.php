<?php // content="text/plain; charset=utf-8"

include '/home/projects/website/testing_center_conf/config.php';
include '/home/projects/public_html/database/inc/class/database.class.php';
include '/home/projects/public_html/database/inc/class/graphData.class.php';

require_once ('../jpgraph.php');
require_once ('../jpgraph_bar.php');

$year = 2013;

$graphData = new GraphData();
$graphData->getWorksBuggedFixed();

//print_r($graphData->tick_num_month);
//print_r($graphData->tick_unnum_month);

//$datay=array(62,105,85,50,50,50,50,50,50,50,50,50);
//$datay=array($graphData->getReportPerMonth(2013));
$datay=$graphData->num_per_col_wbf;

// Create the graph. These two calls are always required
$graph = new Graph(420,350,'auto');
$graph->SetScale("textlin");

//$theme_class="DefaultTheme";
//$graph->SetTheme(new $theme_class());

// set major and minor tick positions manually
$graph->yaxis->SetTickPositions($graphData->tick_num_wbf);
$graph->SetBox(false);

//$graph->ygrid->SetColor('gray');
$graph->ygrid->SetFill(false);
$graph->xaxis->SetTickLabels(array('Works','Bugged','Fixed'));
$graph->yaxis->HideLine(false);
$graph->yaxis->HideTicks(false,false);

// Create the bar plots
$b1plot = new BarPlot($datay);

// ...and add it to the graPH
$graph->Add($b1plot);

$b1plot->value->Show(); 
$b1plot->SetColor("white");
//$b1plot->SetFillGradient("#4B0082","white",GRAD_LEFT_REFLECTION);
$b1plot->SetWidth(45);
$graph->title->Set("Working, bugged and fixed reports (not entries)");

// Display the graph
$graph->Stroke();
?>