<?php
	include 'inc/include.php';

$questsort = new QuestSort;

$phase = new Phase;
$quest = new Quest;

$questsort->getMiscQuestSortIdFromMiscId($_GET['misc']);

if($questsort->misc_sort != false)
{
	$data = array('zoneorsort' => $questsort->misc_sort, 'RequiredClasses' => null);
	$quest->getQuests(1, $data);

	$questsort->getSortInfoBySort($questsort->misc_sort, 'misc');
}

print_r($questsort->misc_sort);
?>
<html>
<head>
	<title>Misc: <?php echo $questsort->sort_list[0]['sort_name']; ?> (<?php echo substr($questsort->sort_list[0]['sort_id'], 1); ?>)</title>
	<link rel="stylesheet" type="text/css" href="css/tracker.css">
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/tabs.js"></script>
</head>
<body>
<div id="t_wrapper">
	<?php include 'inc/menu.php'; ?>
	<?php include 'inc/precontents.php'; ?>
	<div id="content-wrapper">
		<div id="main-content">
			<?php if($questsort->misc_sort != false): ?>
				<section id="quickfacts">
					<h2>Quick Facts</h2>
					<ul>
						<li>Quests: <?php echo $questsort->sort_list[0]['numQuests']; ?></li>
						<li>Quest Progress (Tested): <?php echo $questsort->sort_list[0]['questProgPct']; ?>%</li>
					</ul>
				</section>
				<div id="content-left">
					<h1><?php echo $questsort->sort_list[0]['sort_name']; ?> (<?php echo substr($questsort->sort_list[0]['sort_id'], 1); ?>)</h1>
				</div>
			<?php else: ?>
				<div id="entry_not_found">The requested entry was not found.</div>
			<?php endif; ?>
		</div>
		<?php if($questsort->misc_sort != false): ?>
			<div id="related">
				<div id="tabs-container">
					<ul>
						<li><a href="#comments" class="showSingle" data-target="1">Quests (<?php echo $questsort->sort_list[0]['numQuests']; ?>)</a></li>
					</ul>
					<div class="clear"></div>
				</div>
				<div id="tabs-content">
					<div id="tabs-content-1" class="targetDiv">
						<table id="latest-report-table">
						<?php if($quest->numResults > 0): ?>
						<th><div><span>ID</span></div></th><th><div><span>Quest</span></div></th><th><div><span>Status</span></div></th><th><div><span>Reports</span></div></th>
						<?php foreach($quest->quest as $row): ?>
							<?php
							$report = new Report;
							$report->getReports(1, $row['entry'], QUEST);
							$quest->getStatus($row['entry']);
							?>
							<tr>
								<td><?php echo $row['entry']; ?></td>
								<td><a href="quest.php?quest=<?php echo $row['entry']; ?>"><?php echo $row['Title']; ?></a></td>
								<td><?php echo $quest->status; ?></td>
								<td><?php echo $report->numResults; ?></td>
							</tr>
						<?php endforeach; ?>
						<?php else: ?>
							<p>There are no quests in this zone.</p>
						<?php endif; ?>
						</table>
					</div>
					<div class="clear"></div>
				</div>
			</div>
		<?php endif; ?>
		<div class="clear"></div>
	</div>
</div>