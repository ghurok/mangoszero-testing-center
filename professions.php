<?php
include 'inc/include.php';

$questsort = new QuestSort;
$quest = new Quest;

$questsort->getSortInfoBySort($questsort->getProfQuestSortId(), 'skill');

?>
<!DOCTYPE html>
<html>
<head>
	<title>Professions</title>
	<link rel="stylesheet" type="text/css" href="css/tracker.css">
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/tabs.js"></script>
</head>
<body>
	<div id="t_wrapper">
		<?php include 'inc/menu.php'; ?>
		<?php include 'inc/precontents.php'; ?>
		<div id="content-wrapper">
			<h1>Professions</h1>
			<p>There are no quests for Mining, Skinning nor Enchanting.</p>
			<div id="related">
				<div id="tabs-content">
					<div id="tabs-content-1" class="targetDiv">
						<table>
							<th><div><span>Class</span></div></th><th><div><span>Quests</span></div></th><th colspan="2"><div><span>Quest Progress (Tested)</span></div></th>
							<?php foreach($questsort->sort_list as $row): ?>
								<tr><td><a href="skill.php?skill=<?php echo $row['id']; ?>"><?php echo $row['sort_name']; ?></a></td><td><?php echo $row['numQuests']; ?></td><td><div class="meter"><span style="width: <?php echo $row['questProgPct']; ?>%"></span></div></td><td class="tb-quest-prog-pct"><?php echo $row['questProgPct']; ?>%</td></tr>
							<?php endforeach; ?>
							<tr><td><span class="bold">Total</span></td><td><span class="bold"><?php echo $questsort->sort_total['numQuests']; ?></span></td><td><div class="meter"><span style="width: <?php echo $questsort->sort_total['questProgPct']; ?>%"></span></div></td><td class="tb-quest-prog-pct"><span class="bold"><?php echo $questsort->sort_total['questProgPct']; ?>%</span></td></tr>
						</table>
					</div>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</body>
</html>