<?php

include 'inc/include.php';

if(isset($_GET['user']))
{
	$user->getUser('`id`='.$_GET['user']);
	$user->getCharacters($_GET['user']);

	$report = new Report;
	$report->getReports(null, false, false, true, '`user_id`='.$_GET['user'], '`id` DESC');
	if($report->numResults > 0)
		$latest_reports = array_slice($report->report, -20);
}

?>
<!DOCTYPE html>
<html>
<head>
	<title><?php echo $user->user[0]['username']; ?>'s Profile</title>
	<link rel="stylesheet" type="text/css" href="css/tracker.css">
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/tabs.js"></script>
</head>
<body>
	<div id="t_wrapper">
		<?php include 'inc/menu.php'; ?>
		<?php include 'inc/precontents.php'; ?>
		<div id="content-wrapper">
			<section id="quickfacts">
				<h2>Quick Facts</h2>
				<ul>
					<li>Reports: <?php echo $report->numResults; ?></li>
					<li>Report characters written: <?php echo $report->total_report_char; ?></li>
					<li>In-Game Characters: <?php echo $user->characters['numChars']; ?></li>
				</ul>
			</section>
			<div id="content-left">
				<h1><?php echo $user->user[0]['username']; ?>'s Profile - <?php echo $user->user[0]['online']; ?></h1>
			</div>
			<div id="related">
				<div id="tabs-container">
					<ul>
						<li><a href="#1" class="showSingle" data-target="1">Reports (<?php echo $report->numResults; ?>)</a></li>
						<li><a href="#2" class="showSingle" data-target="2">Reports per month</a></li>
						<li><a href="#3" class="showSingle" data-target="3">Achievements</a></li>
						<li><a href="#4" class="showSingle" data-target="4">Characters (<?php echo $user->characters['numChars']; ?>)</a></li>
					</ul>
					<div class="clear"></div>
				</div>
				<div id="tabs-content">
					<div id="tabs-content-1" class="targetDiv">
					<table id="latest-report-table">
					<?php if($report->numResults > 0): ?>
					<th><div><span>Link</span></div></th><th><div><span>Date</span></div></th><th><div><span>Type</span></div></th><th><div><span>Status</span></div></th><th><div><span>Comment</span></div></th>
						<?php foreach($report->report as $row): ?>
								<tr><td><a href="<?php echo $row['type_name']; ?>.php?<?php echo $row['type_name']; ?>=<?php echo $row['entry']; ?>">Link</a></td><td><?php echo $row['date']; ?></td><td><?php echo ucfirst($row['type_name']); ?></td><td class="status-td"><?php echo $row['status']; ?></td><td class="normal"><?php echo $row['comment']; ?></td></tr>
						<?php endforeach; ?>
					<?php else: ?>
						<p>No reports have been made by <?php echo $user->user[0]['username']; ?></p>
					<?php endif; ?>
					</table>
					</div>
					<div id="tabs-content-2" class="targetDiv">
						<img alt="" src="jpgraph/custom/report_per_month_bars.php?user=<?php echo $_GET['user']; ?>">
					</div>
					<div id="tabs-content-3" class="targetDiv">
						<p>Possibly implemented soon!</p>
					</div>
					<div id="tabs-content-4" class="targetDiv">
					<table>
					<?php if($user->characters['numChars'] > 0): ?>
					<th><div><span>Character</span></div></th><th><div><span>Level</span></div></th><th><div><span>Race</span></div></th><th><div><span>Class</span></div></th><th><div><span>Gender</span></div></th><th><div><span>Online/Offline</span></div></th>
						<?php foreach($user->characters['characters'] as $row): ?>
								<tr><td><?php echo $row['name'];?></td><td><?php echo $row['level']; ?></td><td><?php echo $row['race']; ?></td><td><?php echo $row['class']; ?></td><td><?php echo $row['gender']; ?></td><td class="normal"><?php echo $row['online']; ?></td></tr>
						<?php endforeach; ?>
					<?php else: ?>
						<p>No characters exist for <?php echo $user->user[0]['username']; ?></p>
					<?php endif; ?>
					</table>
					</div>
					<div class="clear"></div>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</body>
</html>