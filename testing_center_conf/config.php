<?php
/* 
	DB_* = mangoszero world database
	DB_*1 = testing center database (sql/website)
	DB_*2 = was used with the Silverpine forum (fluxbb)
	DB_*3 = mangoszero realm database
	DB_*4 = mangoszero character database
*/

if($_SERVER['SERVER_NAME'] == '127.0.0.1')
{
	define('DB_TYPE', 'mysql');
	define('DB_HOST', '127.0.0.1');
	define('DB_USER', 'root');
	define('DB_PASS', '');
	define('DB_NAME', 'zp_world');

	define('DB_TYPE1', 'mysql');
	define('DB_HOST1', '127.0.0.1');
	define('DB_USER1', 'root');
	define('DB_PASS1', '');
	define('DB_NAME1', 'website');

	define('DB_TYPE2', 'mysql');
	define('DB_HOST2', '127.0.0.1');
	define('DB_USER2', 'root');
	define('DB_PASS2', '');
	define('DB_NAME2', 'forums');

	define('DB_TYPE3', 'mysql');
	define('DB_HOST3', '127.0.0.1');
	define('DB_USER3', 'root');
	define('DB_PASS3', '');
	define('DB_NAME3', 'zp_realm');

	define('DB_TYPE4', 'mysql');
	define('DB_HOST4', '127.0.0.1');
	define('DB_USER4', 'root');
	define('DB_PASS4', '');
	define('DB_NAME4', 'zp_characters');
}else
{
	define('DB_TYPE', 'mysql');
	define('DB_HOST', '127.0.0.1');
	define('DB_USER', 'root');
	define('DB_PASS', '');
	define('DB_NAME', 'zp_world');

	define('DB_TYPE1', 'mysql');
	define('DB_HOST1', '127.0.0.1');
	define('DB_USER1', 'root');
	define('DB_PASS1', '');
	define('DB_NAME1', 'website');

	define('DB_TYPE2', 'mysql');
	define('DB_HOST2', '127.0.0.1');
	define('DB_USER2', 'root');
	define('DB_PASS2', '');
	define('DB_NAME2', 'forums');

	define('DB_TYPE3', 'mysql');
	define('DB_HOST3', '127.0.0.1');
	define('DB_USER3', 'root');
	define('DB_PASS3', '');
	define('DB_NAME3', 'zp_realm');

	define('DB_TYPE4', 'mysql');
	define('DB_HOST4', '127.0.0.1');
	define('DB_USER4', 'root');
	define('DB_PASS4', '');
	define('DB_NAME4', 'zp_characters');
}

// Statuses
define("INCORRECT_ASSESSMENT", -3);
define("UNTESTED", -2);
define("NEEDS_MORE_TESTING", -1);

define("DISCUSSION", 0);
define("FIXED", 1);

define("ITEM_NOT_WORKING_AS_INTENDED", 2);
define("ITEM_NO_BUGS_FOUND", 3);
define("ITEM_WORKING_PERFECTLY", 4);
define("ITEM_OBSOLETE", 5);

define("SPELL_NOT_WORKING_AS_INTENDED", 6);
define("SPELL_NO_BUGS_FOUND", 7);
define("SPELL_OBSOLETE", 8);

define("OBJECT_NOT_WORKING_AS_INTENDED", 9);
define("OBJECT_NO_BUGS_FOUND", 10);
define("OBJECT_OBSOLETE", 11);
define("OBJECT_WORKING_PERFECTLY", 12);

define("NPC_NOT_WORKING_AS_INTENDED", 13);
define("NPC_NO_BUGS_FOUND", 14);
define("NPC_OBSOLETE", 15);
define("NPC_WORKING_PERFECTLY", 16);

define("QUEST_BUGS_FOUND", 17);
define("QUEST_NOT_COMPLETABLE", 18);
define("QUEST_COMPLETABLE_NO_BUGS_FOUND", 19);
define("QUEST_OBSOLETE", 20);
define("QUEST_WORKING_PERFECTLY", 21);

// Status Texts
define("UNTESTED_TEXT", 'Untested');
define("NOT_WORKING_AS_INTENDED", 'Not working as intended');
define("NO_BUGS_FOUND", 'No bugs found');
define("WORKING_PERFECTLY", 'Working Perfectly');
define("NEEDS_MORE_TESTING_TEXT", 'Needs more testing');
define("OBSOLETE", 'Obsolete');
define("FIXED_TEXT", 'Fixed');
define("BUGS_FOUND", 'Bugs found');
define("NOT_COMPLETABLE", 'Not Completable');
define("COMPLETABLE_NO_BUGS_FOUND", 'Completable, no bugs found');
define("INCORRECT_ASSESSMENT_TEXT", 'Incorrect Assessment');

// Status Requirements
define("ITEM_NOT_WORKING_AS_INTENDED_MIN", 1);
define("ITEM_NO_BUGS_FOUND_MIN", 3);
define("ITEM_WORKING_PERFECTLY_MIN", 2);
define("ITEM_OBSOLETE_MIN", 2);

define("SPELL_NOT_WORKING_AS_INTENDED_MIN", 1);
define("SPELL_NO_BUGS_FOUND_MIN", 3);
define("SPELL_OBSOLETE_MIN", 2);

define("OBJECT_NOT_WORKING_AS_INTENDED_MIN", 1);
define("OBJECT_NO_BUGS_FOUND_MIN", 3);
define("OBJECT_WORKING_PERFECTLY_MIN", 2);
define("OBJECT_OBSOLETE_MIN", 2);

define("NPC_NOT_WORKING_AS_INTENDED_MIN", 1);
define("NPC_NO_BUGS_FOUND_MIN", 3);
define("NPC_WORKING_PERFECTLY_MIN", 2);
define("NPC_OBSOLETE_MIN", 2);

define("QUEST_BUGS_FOUND_MIN", 1);
define("QUEST_NOT_COMPLETABLE_MIN", 1);
define("QUEST_COMPLETABLE_NO_BUGS_FOUND_MIN", 10);
define("QUEST_OBSOLETE_MIN", 2);
define("QUEST_WORKING_PERFECTLY_MIN", 2);

// Types
define("ITEM", 1);
define("NPC", 2);
define("_OBJECT", 4);
define("SPELL", 8);
define("QUEST", 16);

define("ITEM_NAME", 'item');
define("NPC_NAME", 'npc');
define("_OBJECT_NAME", 'object');
define("SPELL_NAME", 'spell');
define("QUEST_NAME", 'quest');

// Continents
$continents = array(
	'Eastern Kingdoms' => 0,
	'Kalimdor' => 1
	);

// Session Save Directory, not used by default
//define('SESSION_SAVE_DIR', '');
?>